FROM golang:1.22-alpine

ADD . /go/src/myapp

WORKDIR /go/src/myapp

COPY . .

RUN go mod download && go mod verify

RUN go build -o main app/cmd/main.go

CMD ["./main"]


EXPOSE 8080
EXPOSE 5432

