package json_time

import (
	"time"

	jsontime "github.com/liamylian/jsontime/v2/v2"
)

var Json = jsontime.ConfigWithCustomTimeFormat

func init() {
	jsontime.SetDefaultTimeFormat("2006-01-02", time.Local)
}
