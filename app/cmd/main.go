package main

import (
	"context"
	"log"
	"os"
	"os/signal"

	"library/app/internal/config"
	"library/app/internal/core"
)

// @title           Library
// @version         1.0
// @description     It's task of Kata academy.
// @termsOfService  http://swagger.io/terms/

// @host      localhost:8080

// @securityDefinitions.basic  BasicAuth

// @externalDocs.description  task KataAkademy
// @externalDocs.url          https://swagger.io/resources/open-api/
func main() {
	cfg, err := config.GetConfig()
	if err != nil {
		log.Fatal(err.Error())
	}

	app := core.NewApp(cfg)

	app.Run()
	defer app.Shutdown()

	log.Println("server started")

	app.InsertStartData(context.Background(), cfg)

	log.Println("Start Data inserted")
	// Graceful shutdown
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)
	<-sigint
}

//TODO: исправить тесты в сервисе. Добавить пару тестов в хэндлерах.
