package user

import (
	"context"
	"library/app/internal/models/domain/user"
)

func (r *Repo) InsertUsersAndReturnID(ctx context.Context, user user.User) (int, error) {
	const query = `
		INSERT INTO library.users (first_name, middle_name, last_name, birthday)
		VALUES ($1, $2, $3, $4)
		RETURNING id
`

	row := r.db.QueryRow(ctx, query, user.FirstName, user.MiddleName, user.LastName, user.Birthday)

	var userID int
	err := row.Scan(&userID)
	if err != nil {
		return 0, err
	}

	return userID, nil
}
