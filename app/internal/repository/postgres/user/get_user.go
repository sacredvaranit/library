package user

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v5"
	user_db "library/app/internal/models/db/user"
	"library/app/internal/models/domain/user"
)

func (r *Repo) GetUser(ctx context.Context, searchUser user.User) (user.User, error) {
	const query = `
		SELECT users.id, users.first_name, users.middle_name, users.last_name, users.birthday
		FROM library.users
		WHERE users.first_name=$1 AND users.middle_name=$2 AND users.last_name=$3
`

	rows, err := r.db.Query(ctx, query, searchUser.FirstName, searchUser.MiddleName, searchUser.LastName)
	if err != nil {
		return user.User{}, err
	}

	row, err := pgx.CollectOneRow(rows, pgx.RowToStructByNameLax[user_db.Row])
	if err != nil {
		switch {
		case errors.Is(err, pgx.ErrNoRows):
			return user.User{}, nil
		default:
			return user.User{}, err
		}
	}

	return row.ToModel(), nil
}
