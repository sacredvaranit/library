package user

import (
	"context"

	"library/app/internal/models/db/book_author"
	user_db "library/app/internal/models/db/user"
	"library/app/internal/models/domain/user_book_aggregator"
)

func (r *Repo) SelectUsers(ctx context.Context, userID int) ([]user_book_aggregator.UserRentedBookAggregator, error) {
	const query = `
		WITH rented_now AS (
			SELECT rb.user_id, b.name, a.first_name, a.middle_name, a.last_name
			FROM library.rented_books rb
			LEFT JOIN library.users u
			ON u.id=rb.user_id
			LEFT JOIN library.books b
			ON b.id=rb.book_id
			LEFT JOIN library.authors a
			ON a.id=b.author_id
			WHERE rb.returned_at IS NULL AND ($1=0 OR u.id=$1)
		)

		SELECT u.id, u.first_name, u.middle_name, u.last_name, u.birthday,
			COALESCE(rn.name,''), COALESCE(rn.first_name,''),
			COALESCE(rn.middle_name,''), COALESCE(rn.last_name,'')
		FROM library.users u
		LEFT JOIN rented_now rn
		ON rn.user_id=u.id
		WHERE $1=0 OR u.id=$1
`

	rows, err := r.db.Query(ctx, query, userID)
	if err != nil {
		return nil, err
	}

	users := make([]user_book_aggregator.UserRentedBookAggregator, 0)
	var currentID int
	var currentUser user_db.UserBooksAggregator

	for rows.Next() {
		var user user_db.UserBooksAggregator
		var book book_author.Row
		err := rows.Scan(&user.User.ID, &user.User.FirstName, &user.User.MiddleName, &user.User.LastName,
			&user.User.Birthday,
			&book.Name, &book.FirstName, &book.MiddleName, &book.LastName)
		if err != nil {
			return nil, err
		}

		if int(user.User.ID.Int64) != currentID {
			if currentID != 0 {
				users = append(users, currentUser.ToModel())
			}
			currentID = int(user.User.ID.Int64)
			currentUser = user
		}

		if book.Name.String != "" {
			currentUser.Books = append(currentUser.Books, book)
		}
	}
	if int(currentUser.User.ID.Int64) != 0 {
		users = append(users, currentUser.ToModel())
	}

	return users, nil
}
