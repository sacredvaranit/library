package rented_books

import (
	"context"
	"time"
)

func (r *Repo) ReturnedBookAndReturnID(ctx context.Context, userID, bookID int, timeReturn time.Time) (int, error) {
	query := `
		UPDATE library.rented_books
		SET returned_at = $1
		WHERE user_id = $2 AND book_id = $3 AND returned_at IS NULL
		RETURNING id
`

	row := r.db.QueryRow(ctx, query, timeReturn, userID, bookID)

	var entryID int
	if err := row.Scan(&entryID); err != nil {

		return 0, err
	}
	if err := r.updateIssued(ctx, true, bookID); err != nil {

		return 0, err
	}

	return entryID, nil
}
