package rented_books

import (
	"context"
	"time"
)

func (r *Repo) InsertToRentedAndReturnID(
	ctx context.Context,
	userID, bookID int,
	timeCreate,
	timeExpired time.Time) (int, error) {
	const query = `
		INSERT INTO library.rented_books(user_id, book_id, issued_at, expired_at)
		VALUES ($1, $2, $3, $4)
		RETURNING id
`
	row := r.db.QueryRow(ctx, query, userID, bookID, timeCreate, timeExpired)

	var entryID int

	err := row.Scan(&entryID)
	if err != nil {

		return 0, err
	}

	if err = r.updateIssued(ctx, false, bookID); err != nil {

		return 0, err
	}
	return entryID, nil
}
