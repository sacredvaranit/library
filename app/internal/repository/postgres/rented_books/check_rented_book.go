package rented_books

import (
	"context"
)

func (r *Repo) CheckRentedBook(ctx context.Context, userID, bookID int) (bool, error) {
	const query = `
		SELECT EXISTS
		    (SELECT rented_books.user_id, rented_books.book_id
			FROM library.rented_books
			WHERE rented_books.user_id=$1 AND rented_books.book_id=$2 AND rented_books.returned_at IS NULL
			)
`

	row := r.db.QueryRow(ctx, query, userID, bookID)

	var isAvaliable bool

	err := row.Scan(&isAvaliable)
	if err != nil {
		return false, err
	}

	return isAvaliable, nil
}
