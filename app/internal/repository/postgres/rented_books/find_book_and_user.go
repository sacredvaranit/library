package rented_books

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v5"
	book_row "library/app/internal/models/db/book"
	user_row "library/app/internal/models/db/user"
	"library/app/internal/models/domain/book"
	"library/app/internal/models/domain/user"
)

func (r *Repo) FindUserByID(ctx context.Context, userID int) (user.User, error) {
	const query = `
		SELECT id, first_name, middle_name, last_name, birthday
		FROM library.users
		WHERE id=$1
`

	rows, err := r.db.Query(ctx, query, userID)
	if err != nil {
		return user.User{}, err
	}

	row, err := pgx.CollectOneRow(rows, pgx.RowToStructByNameLax[user_row.Row])
	if err != nil {
		switch {
		case errors.Is(err, pgx.ErrNoRows):
			return user.User{}, nil
		default:
			return user.User{}, err
		}
	}

	return row.ToModel(), nil
}

func (r *Repo) FindBookByID(ctx context.Context, bookID int) (book.Book, error) {
	const query = `
		SELECT id, name, author_id, is_issued
		FROM library.books
		WHERE id = $1
`

	rows, err := r.db.Query(ctx, query, bookID)
	if err != nil {
		return book.Book{}, err
	}

	row, err := pgx.CollectOneRow(rows, pgx.RowToStructByNameLax[book_row.Row])
	if err != nil {
		switch {
		case errors.Is(err, pgx.ErrNoRows):
			return book.Book{}, nil
		default:
			return book.Book{}, err
		}
	}

	return row.ToModel(), nil
}
