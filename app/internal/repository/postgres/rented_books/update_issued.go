package rented_books

import "context"

func (r *Repo) updateIssued(ctx context.Context, isIssued bool, bookID int) error {
	const query = `
		UPDATE library.books
		SET is_issued = $1
		WHERE id = $2
`

	_, err := r.db.Exec(ctx, query, isIssued, bookID)
	if err != nil {
		return err
	}

	return nil
}
