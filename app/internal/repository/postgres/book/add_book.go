package add_book_db

import (
	"context"
	"library/app/internal/models/domain/book"
)

func (a *Repo) AddBookAndReturnID(ctx context.Context, book book.Book) (int, error) {
	const query = `
		INSERT INTO library.books(name, author_id)
		VALUES ($1, $2)
		RETURNING id
`

	row := a.db.QueryRow(ctx, query, book.Name, book.AuthorID)

	var bookID int

	err := row.Scan(&bookID)
	if err != nil {

		return 0, err
	}

	return bookID, nil
}
