package add_book_db

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v5"
	author_db "library/app/internal/models/db/author"
	"library/app/internal/models/domain/author"
)

func (r *Repo) GetAuthorByID(ctx context.Context, authorID int) (author.Author, error) {
	const query = `
		SELECT id, first_name, middle_name, last_name
		FROM library.authors
		WHERE id=$1

`

	rows, err := r.db.Query(ctx, query, authorID)
	if err != nil {
		return author.Author{}, err
	}

	row, err := pgx.CollectOneRow(rows, pgx.RowToStructByNameLax[author_db.Row])
	if err != nil {
		switch {
		case errors.Is(err, pgx.ErrNoRows):
			return author.Author{}, nil
		default:
			return author.Author{}, err
		}
	}

	return row.ToModel(), nil
}
