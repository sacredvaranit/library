package add_book_db

import (
	"context"
	"github.com/jackc/pgx/v5"
	book_author_db "library/app/internal/models/db/book_author"
	"library/app/internal/models/domain/book_author"
)

/*
func (r *Repo) GetBooks(ctx context.Context) ([]book_author.BookAuthorAggregator, error) {
	const query = `
		SELECT books.id, books.name, authors.first_name, authors.middle_name, authors.last_name
		FROM library.books
		JOIN library.authors
		ON authors.id=books.author_id
		ORDER BY books.created_at DESC
`

	rows, err := r.db.Query(ctx, query)
	if err != nil {

		return nil, err
	}

	books := make([]book_author.BookAuthorAggregator, 0)
	for rows.Next() {
		book := book_author_db.Row{}

		err = rows.Scan(&book.Book.AuthorID, &book.Book.Name, &book.Author.FirstName, &book.Author.MiddleName, &book.Author.LastName)
		if err != nil {

			return nil, err
		}

		books = append(books, book.ToModel())
	}

	return books, nil
}

*/

func (r *Repo) GetBooks(ctx context.Context, authorID int) ([]book_author.BookAuthorAggregator, error) {
	const query = `
		SELECT books.id, books.name, authors.first_name, authors.middle_name, authors.last_name
		FROM library.books
		JOIN library.authors
		ON authors.id=books.author_id
		WHERE $1=0 OR books.author_id=$1
		ORDER BY books.created_at DESC
`

	rows, err := r.db.Query(ctx, query, authorID)
	if err != nil {

		return nil, err
	}

	rowsAggregator, err := pgx.CollectRows(rows, pgx.RowToStructByName[book_author_db.Row])
	if err != nil {
		return nil, err
	}

	books := make([]book_author.BookAuthorAggregator, 0, len(rowsAggregator))

	for _, arr := range rowsAggregator {
		books = append(books, arr.ToModel())
	}

	return books, nil
}
