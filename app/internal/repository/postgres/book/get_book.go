package add_book_db

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v5"
	book_row "library/app/internal/models/db/book"
	"library/app/internal/models/domain/book"
)

func (r *Repo) GetBook(ctx context.Context, searchBook book.Book) (book.Book, error) {
	const query = `
		SELECT id, name, author_id FROM library.books
		WHERE books.name=$1
`

	rows, err := r.db.Query(ctx, query, searchBook.Name)
	if err != nil {
		return book.Book{}, err
	}

	row, err := pgx.CollectOneRow(rows, pgx.RowToStructByNameLax[book_row.Row])
	if err != nil {
		switch {
		case errors.Is(err, pgx.ErrNoRows):
			return book.Book{}, nil
		default:
			return book.Book{}, err
		}
	}

	return row.ToModel(), nil
}
