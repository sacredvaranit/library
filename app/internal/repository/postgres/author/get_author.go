package author

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v5"
	author_db "library/app/internal/models/db/author"
	"library/app/internal/models/domain/author"
)

func (r *Repo) GetAuthor(ctx context.Context, authorData author.Author) (author.Author, error) {
	const query = `
		SELECT authors.id, authors.first_name, authors.middle_name, authors.last_name
		FROM library.authors
		WHERE authors.first_name=$1 AND authors.middle_name=$2 AND authors.last_name=$3
`

	rows, err := r.db.Query(ctx, query, authorData.FirstName, authorData.MiddleName, authorData.LastName)

	row, err := pgx.CollectOneRow(rows, pgx.RowToStructByNameLax[author_db.Row])
	if err != nil {
		switch {
		case errors.Is(err, pgx.ErrNoRows):
			return author.Author{}, nil
		default:
			return author.Author{}, err
		}
	}

	return row.ToModel(), nil
}
