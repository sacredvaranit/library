package author

import (
	"context"
	"database/sql"

	author_book_db "library/app/internal/models/db/author_book"
	"library/app/internal/models/db/book"
	"library/app/internal/models/domain/author_book"
)

func (r *Repo) GetAuthors(ctx context.Context, authorID int) ([]author_book.AuthorBooksAggregator, error) {
	query := `
		SELECT authors.id, authors.first_name, authors.middle_name, authors.last_name,
		       ARRAY_AGG(books.name)as books 
		FROM library.authors
		LEFT JOIN library.books
		ON books.author_id = authors.id
		WHERE $1=0 OR authors.id=$1
		GROUP BY  authors.id, authors.first_name, authors.middle_name, authors.last_name
	`

	rows, err := r.db.Query(ctx, query, authorID)
	if err != nil {

		return nil, err
	}

	authors := make([]author_book.AuthorBooksAggregator, 0)

	for rows.Next() {
		author := author_book_db.AuthorBookAggregatorDB{}

		var booksNames []sql.NullString

		err = rows.Scan(
			&author.Author.ID,
			&author.Author.FirstName,
			&author.Author.MiddleName,
			&author.Author.LastName,
			&booksNames,
		)
		if err != nil {

			return nil, err
		}

		books := make([]book.Row, 0, len(booksNames))

		for _, b := range booksNames {
			books = append(books, book.Row{Name: b})
		}

		author.Books = append(author.Books, books...)

		authors = append(authors, author.ToModel())
	}

	return authors, nil
}
