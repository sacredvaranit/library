package author

import (
	"context"
	"database/sql"
	"library/app/internal/models/domain/author"
)

func (a *Repo) AddAuthorAndReturnID(ctx context.Context, author author.Author) (int, error) {
	query := `
		INSERT INTO library.authors (first_name, middle_name, last_name)
		VALUES ($1, $2, $3)
		RETURNING id
	`

	row := a.db.QueryRow(ctx, query, author.FirstName, author.MiddleName, author.LastName)
	var authorID sql.NullInt64

	err := row.Scan(&authorID)
	if err != nil {

		return 0, err
	}

	return int(authorID.Int64), nil
}
