package book

type GetBooksResponse struct {
	ID     int        `json:"ID"`
	Name   string     `json:"bookName"`
	Author AuthorBook `json:"author"`
}

type AuthorBook struct {
	FirstName  string `json:"firstName"`
	MiddleName string `json:"middleName"`
	LastName   string `json:"lastName"`
}
