package book

import "library/app/internal/models/domain/book"

type AddBookRequest struct {
	Name     string `json:"name" validate:"required"`
	AuthorID int    `json:"authorID" validate:"required"`
}

func (r AddBookRequest) ToModel() book.Book {
	return book.Book{
		Name:     r.Name,
		AuthorID: r.AuthorID,
	}
}

type AddBookResponse struct {
	BookID int `json:"bookID"`
}
