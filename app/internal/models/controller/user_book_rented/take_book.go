package user_book_rented

type TakeBookRequest struct {
	UserID int `json:"userID" validate:"required"`
	BookID int `json:"bookID" validate:"required"`
}

type TakeBookResponse struct {
	EntryID int `json:"entryID"`
}
