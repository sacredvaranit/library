package user_book_rented

type ReturnBookRequest struct {
	UserID int `json:"userID" validate:"required"`
	BookID int `json:"bookID" validate:"required"`
}

type ReturnBookResponse struct {
	EntryID int `json:"entryID"`
}
