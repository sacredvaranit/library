package user

import (
	"time"
)

type GetUsersResponse struct {
	ID          int       `json:"id"`
	FirstName   string    `json:"FirstName"`
	MiddleName  string    `json:"middleName"`
	LastName    string    `json:"lastName"`
	Birthday    time.Time `json:"birthday" time_format:"2006-01-02"`
	RentedBooks []Book    `json:"rentedBooks"`
}

type Book struct {
	Name   string `json:"book_name"`
	Author Author
}

type Author struct {
	FirstName  string `json:"first_name"`
	MiddleName string `json:"middle_name"`
	LastName   string `json:"last_name"`
}
