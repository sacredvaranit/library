package user

import (
	"time"

	"library/app/internal/models/domain/user"
)

type UserCreateRequest struct {
	FirstName  string    `json:"firstName" validate:"required"`
	MiddleName string    `json:"middleName"`
	LastName   string    `json:"lastName" validate:"required"`
	Birthday   time.Time `json:"birthday" time_format:"2006-01-02"`
}

func (r *UserCreateRequest) ToModel() user.User {
	return user.User{
		FirstName:  r.FirstName,
		MiddleName: r.MiddleName,
		LastName:   r.LastName,
		Birthday:   r.Birthday,
	}
}

type UserCreateResponse struct {
	UserID int `json:"userID"`
}
