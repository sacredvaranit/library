package author

import (
	"library/app/internal/models/domain/author"
)

type AddAuthorRequest struct {
	FirstName  string `json:"firstName" validate:"required"`
	MiddleName string `json:"middleName"`
	LastName   string `json:"lastName" validate:"required"`
}

func (r AddAuthorRequest) ToModel() author.Author {
	return author.Author{
		FirstName:  r.FirstName,
		MiddleName: r.MiddleName,
		LastName:   r.LastName,
	}
}

type AddAuthorResponse struct {
	AuthorID int `json:"authorID"`
}
