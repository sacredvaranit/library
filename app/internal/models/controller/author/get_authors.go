package author

type GetAuthorsResponse struct {
	ID         int    `json:"id"`
	FirstName  string `json:"firstName"`
	MiddleName string `json:"middleName"`
	LastName   string `json:"lastName"`
	Books      []struct {
		Name string `json:"bookName"`
	} `json:"books"`
}
