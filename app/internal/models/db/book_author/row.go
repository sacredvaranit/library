package book_author

import (
	"database/sql"
	"library/app/internal/models/domain/author"
	"library/app/internal/models/domain/book"
	"library/app/internal/models/domain/book_author"
)

type Row struct {
	ID         sql.NullInt64  `db:"id"`
	Name       sql.NullString `db:"name"`
	FirstName  sql.NullString `db:"first_name"`
	MiddleName sql.NullString `db:"middle_name"`
	LastName   sql.NullString `db:"last_name"`
}

func (r *Row) ToModel() book_author.BookAuthorAggregator {
	return book_author.BookAuthorAggregator{
		Book: book.Book{
			ID:   int(r.ID.Int64),
			Name: r.Name.String,
		},

		Author: author.Author{
			FirstName:  r.FirstName.String,
			MiddleName: r.MiddleName.String,
			LastName:   r.LastName.String,
		},
	}
}
