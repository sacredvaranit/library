package author_book

import (
	author_db "library/app/internal/models/db/author"
	"library/app/internal/models/db/book"
	"library/app/internal/models/domain/author"
	"library/app/internal/models/domain/author_book"
	book_domain "library/app/internal/models/domain/book"
)

type AuthorBookAggregatorDB struct {
	Author author_db.Row
	Books  []book.Row
}

type BookOfAuthorDB struct {
	BookName string
}

func (a AuthorBookAggregatorDB) ToModel() author_book.AuthorBooksAggregator {
	return author_book.AuthorBooksAggregator{
		Author: author.Author{
			ID:         int(a.Author.ID.Int64),
			FirstName:  a.Author.FirstName.String,
			MiddleName: a.Author.MiddleName.String,
			LastName:   a.Author.LastName.String,
		},

		Books: func() []book_domain.Book {
			books := make([]book_domain.Book, 0)

			for _, b := range a.Books {
				books = append(books, book_domain.Book{Name: b.Name.String})
			}

			return books
		}(),
	}
}
