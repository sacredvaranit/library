package book

import (
	"database/sql"
	"library/app/internal/models/domain/book"
)

type Row struct {
	ID       sql.NullInt64  `db:"id"`
	Name     sql.NullString `db:"name"`
	AuthorID sql.NullInt64  `db:"author_id"`
	IsIssued sql.NullBool   `db:"is_issued"`
}

func (r Row) ToModel() book.Book {
	return book.Book{
		ID:       int(r.ID.Int64),
		Name:     r.Name.String,
		AuthorID: int(r.AuthorID.Int64),
		IsIssued: r.IsIssued.Bool,
	}
}
