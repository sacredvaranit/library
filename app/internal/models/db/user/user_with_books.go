package user

import (
	book_author_db "library/app/internal/models/db/book_author"
	"library/app/internal/models/domain/author"
	"library/app/internal/models/domain/book"
	"library/app/internal/models/domain/book_author"
	"library/app/internal/models/domain/user"
	"library/app/internal/models/domain/user_book_aggregator"
)

type UserBooksAggregator struct {
	User  Row
	Books []book_author_db.Row
}

func (ub UserBooksAggregator) ToModel() user_book_aggregator.UserRentedBookAggregator {
	return user_book_aggregator.UserRentedBookAggregator{
		User: user.User{
			ID:         int(ub.User.ID.Int64),
			FirstName:  ub.User.FirstName.String,
			MiddleName: ub.User.MiddleName.String,
			LastName:   ub.User.LastName.String,
			Birthday:   ub.User.Birthday.Time,
		},

		RentedBooks: func() []book_author.BookAuthorAggregator {
			books := make([]book_author.BookAuthorAggregator, 0)
			for _, useBook := range ub.Books {
				books = append(books, book_author.BookAuthorAggregator{
					Book: book.Book{
						Name: useBook.Name.String,
					},
					Author: author.Author{
						FirstName:  useBook.FirstName.String,
						MiddleName: useBook.MiddleName.String,
						LastName:   useBook.LastName.String,
					},
				})
			}

			return books
		}(),
	}
}
