package user

import (
	"database/sql"
	"library/app/internal/models/domain/user"
)

type Row struct {
	ID         sql.NullInt64  `db:"id"`
	FirstName  sql.NullString `db:"first_name"`
	MiddleName sql.NullString `db:"middle_name"`
	LastName   sql.NullString `db:"last_name"`
	Birthday   sql.NullTime   `db:"birthday"`
}

func (r *Row) ToModel() user.User {
	return user.User{
		ID:         int(r.ID.Int64),
		FirstName:  r.FirstName.String,
		MiddleName: r.MiddleName.String,
		LastName:   r.LastName.String,
		Birthday:   r.Birthday.Time,
	}
}
