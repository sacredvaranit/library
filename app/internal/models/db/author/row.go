package author

import (
	"database/sql"
	"library/app/internal/models/domain/author"
)

type Row struct {
	ID         sql.NullInt64  `DB:"id"`
	FirstName  sql.NullString `db:"first_name"`
	MiddleName sql.NullString `db:"middle_name"`
	LastName   sql.NullString `db:"last_name"`
}

func (r *Row) ToModel() author.Author {
	return author.Author{
		ID:         int(r.ID.Int64),
		FirstName:  r.FirstName.String,
		MiddleName: r.MiddleName.String,
		LastName:   r.LastName.String,
	}
}
