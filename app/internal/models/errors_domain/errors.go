package errors_domain

type NotFoundError struct {
	error
}

type DomainError struct {
	error
}

func NewDomainError(err error) DomainError {
	return DomainError{err}
}

func NewNotFoundError(err error) NotFoundError {
	return NotFoundError{err}
}
