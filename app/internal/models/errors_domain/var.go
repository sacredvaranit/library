package errors_domain

import "errors"

var (
	ErrUserNotFound = NewNotFoundError(errors.New("User is not found"))

	ErrAuthorExist = NewDomainError(errors.New("Author is already exist"))

	ErrBookExist = NewDomainError(errors.New("Book is already exist"))

	ErrUserExist = NewDomainError(errors.New("User is already exist"))

	ErrBookNotFound = NewNotFoundError(errors.New("Book is not found"))

	ErrBookAlreadyRent = NewDomainError(errors.New("Book is already rent by user"))

	ErrAuthorNotFound = NewNotFoundError(errors.New("Author not found"))

	ErrNoRentedBookByUserErr = NewNotFoundError(errors.New("Not found rented book by this user. Check your params"))

	ErrWrongBodyRequest = NewDomainError(errors.New("Wrong body for request!"))
)
