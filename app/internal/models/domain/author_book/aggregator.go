package author_book

import (
	"library/app/internal/models/domain/author"
	"library/app/internal/models/domain/book"
)

type AuthorBooksAggregator struct {
	Author author.Author
	Books  []book.Book
}
