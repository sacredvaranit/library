package user

import (
	"time"
)

type User struct {
	ID         int
	FirstName  string
	MiddleName string
	LastName   string
	Birthday   time.Time
}
