package user_book_aggregator

import (
	"library/app/internal/models/domain/book_author"
	"library/app/internal/models/domain/user"
)

type UserRentedBookAggregator struct {
	User        user.User
	RentedBooks []book_author.BookAuthorAggregator
}
