package book_author

import (
	"library/app/internal/models/domain/author"
	"library/app/internal/models/domain/book"
)

type BookAuthorAggregator struct {
	Book   book.Book
	Author author.Author
}
