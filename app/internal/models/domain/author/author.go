package author

type Author struct {
	ID         int
	FirstName  string
	MiddleName string
	LastName   string
}
