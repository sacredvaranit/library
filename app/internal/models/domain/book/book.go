package book

type Book struct {
	ID       int
	Name     string
	AuthorID int
	IsIssued bool
}
