package user

import (
	"encoding/json"
	"library/app/internal/lib/context_value"
	"library/app/internal/lib/validate"
	"library/app/internal/models/controller/user"
	"net/http"
)

// Create user godoc
// @Summary      Create user
// @Description  You can create a new user and insert him into table. Birthday format: YY-MM-DD
// @Tags         users
// @Accept       json
// @Produce      json
// @Param        input 	body 	user.UserCreateRequest 	true 	"Created user"
// @Success		200		{object} user.UserCreateResponse
// @router       /users [POST]
func (h *Handler) CreateUser(w http.ResponseWriter, r *http.Request) {
	request, err := decodeUserRequest(r.Body)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	if err = validate.ValidateRequest(request); err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	userID, err := h.userService.CreateUser(r.Context(), request.ToModel())
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	response, err := json.Marshal(user.UserCreateResponse{UserID: userID})
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	w.Write(response)
}
