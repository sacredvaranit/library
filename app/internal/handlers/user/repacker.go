package user

import (
	"io"

	user_controller "library/app/internal/models/controller/user"
	"library/app/internal/models/domain/book_author"
	"library/app/internal/models/domain/user_book_aggregator"
	"library/app/pkg/json_time"
)

func decodeUserRequest(r io.Reader) (user_controller.UserCreateRequest, error) {
	var userRequest user_controller.UserCreateRequest
	err := json_time.Json.NewDecoder(r).Decode(&userRequest)
	if err != nil {
		return user_controller.UserCreateRequest{}, err
	}

	return userRequest, nil
}

func usersToResponse(users []user_book_aggregator.UserRentedBookAggregator) []user_controller.GetUsersResponse {
	response := make([]user_controller.GetUsersResponse, 0)
	for _, user := range users {
		response = append(response,
			user_controller.GetUsersResponse{
				ID:          user.User.ID,
				FirstName:   user.User.FirstName,
				MiddleName:  user.User.MiddleName,
				LastName:    user.User.LastName,
				Birthday:    user.User.Birthday,
				RentedBooks: booksToResponse(user.RentedBooks),
			})
	}

	return response
}

func booksToResponse(books []book_author.BookAuthorAggregator) []user_controller.Book {
	responseBooks := make([]user_controller.Book, 0)
	for _, useBook := range books {
		responseBooks = append(responseBooks,
			user_controller.Book{
				Name: useBook.Book.Name,
				Author: user_controller.Author{
					FirstName:  useBook.Author.FirstName,
					MiddleName: useBook.Author.MiddleName,
					LastName:   useBook.Author.LastName,
				},
			},
		)
	}

	return responseBooks
}
