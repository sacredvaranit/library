package user

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	user_http "library/app/internal/models/controller/user"
	error_models "library/app/internal/models/errors_domain"
	"library/app/pkg/json_time"
	"net/http/httptest"
	"testing"
	"time"
)

func TestHandler_CreateUser(t *testing.T) {
	const (
		method = "POST"

		url = "/users"

		id = 1

		firstName = "Антон"

		middleName = "Павлович"

		lastName = "Чехов"
	)

	var (
		birthday = time.Date(2000, time.January, 1, 0, 0, 0, 0, time.Local)

		newUser = user_http.UserCreateRequest{
			FirstName:  firstName,
			MiddleName: middleName,
			LastName:   lastName,
			Birthday:   birthday,
		}

		successResponse = user_http.UserCreateResponse{
			UserID: id,
		}
	)

	createUserResponseBody, _ := json.Marshal(&successResponse)
	createUserRequestBody, _ := json_time.Json.Marshal(&newUser)
	createWrongRequstBody, _ := json.Marshal(&newUser)

	tests := []struct {
		name     string
		setup    func(ctx context.Context, cntrl *minimock.Controller) Handler
		reqBody  []byte
		wantBody []byte
	}{
		{name: "ok",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewUserManagerMock(cntrl)
				mock.CreateUserMock.Expect(ctx, newUser.ToModel()).Return(id, nil)

				return Handler{
					userService: mock,
				}
			},
			reqBody:  createUserRequestBody,
			wantBody: createUserResponseBody,
		},

		{name: "with err in user manager",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewUserManagerMock(cntrl)
				mock.CreateUserMock.Expect(ctx, newUser.ToModel()).Return(0, error_models.ErrUserExist)

				return Handler{
					userService: mock,
				}
			},
			reqBody:  createUserRequestBody,
			wantBody: nil,
		},

		{name: "with err unmarshal",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewUserManagerMock(cntrl)

				return Handler{
					userService: mock,
				}
			},
			reqBody:  createWrongRequstBody,
			wantBody: nil,
		},
		// TODO: Add test cases.
	}

	cntrl := minimock.NewController(t)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testRequest := httptest.NewRequest(method, url, bytes.NewReader(tt.reqBody))
			response := httptest.NewRecorder()

			handler := tt.setup(testRequest.Context(), cntrl)

			handler.CreateUser(response, testRequest)

			assert.Equal(t, tt.wantBody, response.Body.Bytes())
		})
	}
}
