package user

import (
	"context"
	"fmt"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	"library/app/internal/models/domain/author"
	"library/app/internal/models/domain/book"
	"library/app/internal/models/domain/book_author"
	"library/app/internal/models/domain/user"
	"library/app/internal/models/domain/user_book_aggregator"
	error_models "library/app/internal/models/errors_domain"
	"library/app/pkg/json_time"
	"net/http/httptest"
	"testing"
	"time"
)

func TestHandler_GetUsers(t *testing.T) {
	const (
		paramID = "1"

		userID     = 1
		firstName  = "Антон"
		middleName = "Павлович"
		lastName   = "Чехов"

		authorID         = 4
		authorFirstName  = "Василий"
		authorMiddleName = "Сергеевич"
		authorLastName   = "Попов"

		bookID   = 2
		bookName = "Надоели тесты"

		method = "POST"
		url    = "/users?user-id=%s"
	)

	var (
		birthday = time.Date(2000, time.January, 1, 0, 0, 0, 0, time.Local)

		users = []user_book_aggregator.UserRentedBookAggregator{
			{
				User: user.User{
					ID:         userID,
					FirstName:  firstName,
					MiddleName: middleName,
					LastName:   lastName,
					Birthday:   birthday,
				},
				RentedBooks: []book_author.BookAuthorAggregator{
					{Book: book.Book{
						ID:       bookID,
						Name:     bookName,
						AuthorID: 0,
						IsIssued: false,
					},
						Author: author.Author{
							ID:         authorID,
							FirstName:  authorFirstName,
							MiddleName: authorMiddleName,
							LastName:   authorLastName,
						},
					},
				},
			},
		}

		usersResponse = usersToResponse(users)
	)

	succefullResponse, _ := json_time.Json.Marshal(&usersResponse)

	tests := []struct {
		name     string
		setup    func(ctx context.Context, cntrl *minimock.Controller) Handler
		url      string
		queryID  string
		wantBody []byte
	}{
		{
			name: "ok",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewUserManagerMock(cntrl)
				mock.GetUsersMock.Expect(ctx, userID).Return(users, nil)

				return Handler{
					userService: mock,
				}
			},
			url:      url,
			queryID:  paramID,
			wantBody: succefullResponse,
		},

		{
			name: "with err",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewUserManagerMock(cntrl)
				mock.GetUsersMock.Expect(ctx, userID).Return(nil, error_models.ErrUserNotFound)

				return Handler{
					userService: mock,
				}
			},
			url:      url,
			queryID:  paramID,
			wantBody: nil,
		},

		{
			name: "with err param user-id",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewUserManagerMock(cntrl)

				return Handler{
					userService: mock,
				}
			},
			url:      url,
			queryID:  "lol",
			wantBody: nil,
		},
		// TODO: Add test cases.
	}

	cntrl := minimock.NewController(t)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testRequest := httptest.NewRequest(method, fmt.Sprintf(tt.url, tt.queryID), nil)
			response := httptest.NewRecorder()

			handler := tt.setup(testRequest.Context(), cntrl)

			handler.GetUsers(response, testRequest)

			assert.Equal(t, tt.wantBody, response.Body.Bytes())
		})
	}
}
