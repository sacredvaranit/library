package user

import (
	"context"
	"net/http"

	"library/app/internal/models/domain/user"
	"library/app/internal/models/domain/user_book_aggregator"
)

type userManager interface {
	CreateUser(ctx context.Context, user user.User) (int, error)
	GetUsers(ctx context.Context, userID int) ([]user_book_aggregator.UserRentedBookAggregator, error)
}

type Handler struct {
	userService userManager
}

func NewHandler(userService userManager) *Handler {
	return &Handler{userService: userService}
}

type router interface {
	Get(pattern string, handlerFn http.HandlerFunc)
	Post(pattern string, handlerFn http.HandlerFunc)
}

func (h *Handler) Register(r router) {
	r.Post("/users", h.CreateUser)
	r.Get("/users", h.GetUsers)
}
