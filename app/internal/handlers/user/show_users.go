package user

import (
	"library/app/internal/lib/context_value"
	"net/http"
	"net/url"
	"strconv"

	"library/app/pkg/json_time"
)

// Get user godoc
// @Summary      Get user
// @Description  You can get the user with rented books by user's ID. Empty query - get all users
// @Tags         users
// @Accept       json
// @Produce      json
// @Param        user-id    query     string  false  "user's ID"  Format(user-id)
// @Success		200		{array}	user.GetUsersResponse
// @router       /users [GET]
func (h *Handler) GetUsers(w http.ResponseWriter, r *http.Request) {
	const paramKey = "user-id"
	var userID int

	parseURL, err := url.Parse(r.URL.String())
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	params, err := url.ParseQuery(parseURL.RawQuery)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	userID, err = strconv.Atoi(params.Get(paramKey))
	if err != nil && params.Get(paramKey) != "" {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	users, err := h.userService.GetUsers(r.Context(), userID)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	userResponse := usersToResponse(users)

	response, err := json_time.Json.Marshal(&userResponse)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	w.Write(response)
}
