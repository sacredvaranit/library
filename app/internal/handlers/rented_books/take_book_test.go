package rented_books

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	"library/app/internal/models/controller/user_book_rented"
	error_models "library/app/internal/models/errors_domain"
	"net/http/httptest"
	"testing"
)

func TestHandler_TakeBookForUser(t *testing.T) {
	const (
		userID = 1
		bookID = 2
		rentID = 3

		method = "POST"
		url    = "/books/get"
	)

	var (
		req = user_book_rented.TakeBookRequest{
			UserID: userID,
			BookID: bookID,
		}

		resp = user_book_rented.TakeBookResponse{rentID}
	)

	reqBody, _ := json.Marshal(&req)
	respBody, _ := json.Marshal(&resp)

	tests := []struct {
		name     string
		setup    func(ctx context.Context, cntrl *minimock.Controller) Handler
		reqBody  []byte
		wantBody []byte
	}{
		{
			name: "ok",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewRentedBooksManagerMock(cntrl)
				mock.TakeBookMock.Expect(ctx, userID, bookID).Return(rentID, nil)

				return Handler{
					userService: mock,
				}
			},
			reqBody:  reqBody,
			wantBody: respBody,
		},

		{
			name: "with err service",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewRentedBooksManagerMock(cntrl)
				mock.TakeBookMock.Expect(ctx, userID, bookID).Return(0, error_models.ErrBookAlreadyRent)

				return Handler{
					userService: mock,
				}
			},
			reqBody:  reqBody,
			wantBody: nil,
		},

		{
			name: "err decode",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewRentedBooksManagerMock(cntrl)

				return Handler{
					userService: mock,
				}
			},
			reqBody:  nil,
			wantBody: nil,
		},

		// TODO: Add test cases.
	}

	cntrl := minimock.NewController(t)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testRequest := httptest.NewRequest(method, url, bytes.NewReader(tt.reqBody))
			response := httptest.NewRecorder()

			handler := tt.setup(testRequest.Context(), cntrl)

			handler.TakeBookForUser(response, testRequest)

			assert.Equal(t, tt.wantBody, response.Body.Bytes())
		})
	}
}
