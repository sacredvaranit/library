package rented_books

import (
	"context"
	"net/http"
)

type rentedBooksManager interface {
	TakeBook(ctx context.Context, userID, bookID int) (int, error)
	ReturnBook(ctx context.Context, userID, bookID int) (int, error)
}

type Handler struct {
	userService rentedBooksManager
}

func NewHandler(userService rentedBooksManager) *Handler {
	return &Handler{userService: userService}
}

type router interface {
	Get(pattern string, handlerFn http.HandlerFunc)
	Post(pattern string, handlerFn http.HandlerFunc)
}

func (h *Handler) Register(r router) {
	r.Post("/books/return", h.GetBookFromUser)
	r.Post("/books/get", h.TakeBookForUser)
}
