package rented_books

import (
	"encoding/json"
	"library/app/internal/lib/context_value"
	"library/app/internal/lib/validate"
	"library/app/internal/models/controller/user_book_rented"
	"net/http"
)

// Take book godoc
// @Summary      Taking book by user
// @Description  You can emulated take book by user from library
// @Tags         rent
// @Accept       json
// @Produce      json
// @Param        input 	body 	user_book_rented.TakeBookRequest 	true 	"Take book"
// @Success		200		{object}	user_book_rented.TakeBookResponse
// @router       /books/get [POST]
func (u *Handler) TakeBookForUser(w http.ResponseWriter, r *http.Request) {
	ids, err := decodeTakeRequest(r.Body)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	if err = validate.ValidateRequest(ids); err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	entryID, err := u.userService.TakeBook(r.Context(), ids.UserID, ids.BookID)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	response, err := json.Marshal(user_book_rented.TakeBookResponse{EntryID: entryID})
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	w.Write(response)
}
