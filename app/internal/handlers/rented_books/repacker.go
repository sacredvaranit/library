package rented_books

import (
	"encoding/json"
	"io"

	"library/app/internal/models/controller/user_book_rented"
)

func decodeReturnedRequest(r io.Reader) (user_book_rented.ReturnBookRequest, error) {
	var userBookIDs user_book_rented.ReturnBookRequest

	err := json.NewDecoder(r).Decode(&userBookIDs)
	if err != nil {
		return user_book_rented.ReturnBookRequest{}, err
	}

	return userBookIDs, nil
}

func decodeTakeRequest(r io.Reader) (user_book_rented.TakeBookRequest, error) {
	var userBookIDs user_book_rented.TakeBookRequest

	err := json.NewDecoder(r).Decode(&userBookIDs)
	if err != nil {
		return user_book_rented.TakeBookRequest{}, err
	}

	return userBookIDs, nil
}
