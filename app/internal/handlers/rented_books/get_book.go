package rented_books

import (
	"encoding/json"
	"library/app/internal/lib/context_value"
	"library/app/internal/lib/validate"
	"library/app/internal/models/controller/user_book_rented"
	"net/http"
)

// Get book godoc
// @Summary      Get book by user
// @Description  You can emulated return book by user into library
// @Tags         rent
// @Accept       json
// @Produce      json
// @Param        input 	body 	user_book_rented.ReturnBookRequest 	true 	"Return book"
// @Success		200		{object}	user_book_rented.ReturnBookResponse
// @router       /books/return [POST]
func (u *Handler) GetBookFromUser(w http.ResponseWriter, r *http.Request) {
	ids, err := decodeReturnedRequest(r.Body)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	if err = validate.ValidateRequest(ids); err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	entryID, err := u.userService.ReturnBook(r.Context(), ids.UserID, ids.BookID)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	response, err := json.Marshal(user_book_rented.ReturnBookResponse{EntryID: entryID})
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	w.Write(response)
}
