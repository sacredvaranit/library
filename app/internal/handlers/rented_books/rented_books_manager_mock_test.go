// Code generated by http://github.com/gojuno/minimock (v3.3.13). DO NOT EDIT.

package rented_books

//go:generate minimock -i library/app/internal/handlers/rented_books.rentedBooksManager -o rented_books_manager_mock_test.go -n RentedBooksManagerMock -p rented_books

import (
	"context"
	"sync"
	mm_atomic "sync/atomic"
	mm_time "time"

	"github.com/gojuno/minimock/v3"
)

// RentedBooksManagerMock implements rentedBooksManager
type RentedBooksManagerMock struct {
	t          minimock.Tester
	finishOnce sync.Once

	funcReturnBook          func(ctx context.Context, userID int, bookID int) (i1 int, err error)
	inspectFuncReturnBook   func(ctx context.Context, userID int, bookID int)
	afterReturnBookCounter  uint64
	beforeReturnBookCounter uint64
	ReturnBookMock          mRentedBooksManagerMockReturnBook

	funcTakeBook          func(ctx context.Context, userID int, bookID int) (i1 int, err error)
	inspectFuncTakeBook   func(ctx context.Context, userID int, bookID int)
	afterTakeBookCounter  uint64
	beforeTakeBookCounter uint64
	TakeBookMock          mRentedBooksManagerMockTakeBook
}

// NewRentedBooksManagerMock returns a mock for rentedBooksManager
func NewRentedBooksManagerMock(t minimock.Tester) *RentedBooksManagerMock {
	m := &RentedBooksManagerMock{t: t}

	if controller, ok := t.(minimock.MockController); ok {
		controller.RegisterMocker(m)
	}

	m.ReturnBookMock = mRentedBooksManagerMockReturnBook{mock: m}
	m.ReturnBookMock.callArgs = []*RentedBooksManagerMockReturnBookParams{}

	m.TakeBookMock = mRentedBooksManagerMockTakeBook{mock: m}
	m.TakeBookMock.callArgs = []*RentedBooksManagerMockTakeBookParams{}

	t.Cleanup(m.MinimockFinish)

	return m
}

type mRentedBooksManagerMockReturnBook struct {
	optional           bool
	mock               *RentedBooksManagerMock
	defaultExpectation *RentedBooksManagerMockReturnBookExpectation
	expectations       []*RentedBooksManagerMockReturnBookExpectation

	callArgs []*RentedBooksManagerMockReturnBookParams
	mutex    sync.RWMutex

	expectedInvocations uint64
}

// RentedBooksManagerMockReturnBookExpectation specifies expectation struct of the rentedBooksManager.ReturnBook
type RentedBooksManagerMockReturnBookExpectation struct {
	mock      *RentedBooksManagerMock
	params    *RentedBooksManagerMockReturnBookParams
	paramPtrs *RentedBooksManagerMockReturnBookParamPtrs
	results   *RentedBooksManagerMockReturnBookResults
	Counter   uint64
}

// RentedBooksManagerMockReturnBookParams contains parameters of the rentedBooksManager.ReturnBook
type RentedBooksManagerMockReturnBookParams struct {
	ctx    context.Context
	userID int
	bookID int
}

// RentedBooksManagerMockReturnBookParamPtrs contains pointers to parameters of the rentedBooksManager.ReturnBook
type RentedBooksManagerMockReturnBookParamPtrs struct {
	ctx    *context.Context
	userID *int
	bookID *int
}

// RentedBooksManagerMockReturnBookResults contains results of the rentedBooksManager.ReturnBook
type RentedBooksManagerMockReturnBookResults struct {
	i1  int
	err error
}

// Marks this method to be optional. The default behavior of any method with Return() is '1 or more', meaning
// the test will fail minimock's automatic final call check if the mocked method was not called at least once.
// Optional() makes method check to work in '0 or more' mode.
// It is NOT RECOMMENDED to use this option unless you really need it, as default behaviour helps to
// catch the problems when the expected method call is totally skipped during test run.
func (mmReturnBook *mRentedBooksManagerMockReturnBook) Optional() *mRentedBooksManagerMockReturnBook {
	mmReturnBook.optional = true
	return mmReturnBook
}

// Expect sets up expected params for rentedBooksManager.ReturnBook
func (mmReturnBook *mRentedBooksManagerMockReturnBook) Expect(ctx context.Context, userID int, bookID int) *mRentedBooksManagerMockReturnBook {
	if mmReturnBook.mock.funcReturnBook != nil {
		mmReturnBook.mock.t.Fatalf("RentedBooksManagerMock.ReturnBook mock is already set by Set")
	}

	if mmReturnBook.defaultExpectation == nil {
		mmReturnBook.defaultExpectation = &RentedBooksManagerMockReturnBookExpectation{}
	}

	if mmReturnBook.defaultExpectation.paramPtrs != nil {
		mmReturnBook.mock.t.Fatalf("RentedBooksManagerMock.ReturnBook mock is already set by ExpectParams functions")
	}

	mmReturnBook.defaultExpectation.params = &RentedBooksManagerMockReturnBookParams{ctx, userID, bookID}
	for _, e := range mmReturnBook.expectations {
		if minimock.Equal(e.params, mmReturnBook.defaultExpectation.params) {
			mmReturnBook.mock.t.Fatalf("Expectation set by When has same params: %#v", *mmReturnBook.defaultExpectation.params)
		}
	}

	return mmReturnBook
}

// ExpectCtxParam1 sets up expected param ctx for rentedBooksManager.ReturnBook
func (mmReturnBook *mRentedBooksManagerMockReturnBook) ExpectCtxParam1(ctx context.Context) *mRentedBooksManagerMockReturnBook {
	if mmReturnBook.mock.funcReturnBook != nil {
		mmReturnBook.mock.t.Fatalf("RentedBooksManagerMock.ReturnBook mock is already set by Set")
	}

	if mmReturnBook.defaultExpectation == nil {
		mmReturnBook.defaultExpectation = &RentedBooksManagerMockReturnBookExpectation{}
	}

	if mmReturnBook.defaultExpectation.params != nil {
		mmReturnBook.mock.t.Fatalf("RentedBooksManagerMock.ReturnBook mock is already set by Expect")
	}

	if mmReturnBook.defaultExpectation.paramPtrs == nil {
		mmReturnBook.defaultExpectation.paramPtrs = &RentedBooksManagerMockReturnBookParamPtrs{}
	}
	mmReturnBook.defaultExpectation.paramPtrs.ctx = &ctx

	return mmReturnBook
}

// ExpectUserIDParam2 sets up expected param userID for rentedBooksManager.ReturnBook
func (mmReturnBook *mRentedBooksManagerMockReturnBook) ExpectUserIDParam2(userID int) *mRentedBooksManagerMockReturnBook {
	if mmReturnBook.mock.funcReturnBook != nil {
		mmReturnBook.mock.t.Fatalf("RentedBooksManagerMock.ReturnBook mock is already set by Set")
	}

	if mmReturnBook.defaultExpectation == nil {
		mmReturnBook.defaultExpectation = &RentedBooksManagerMockReturnBookExpectation{}
	}

	if mmReturnBook.defaultExpectation.params != nil {
		mmReturnBook.mock.t.Fatalf("RentedBooksManagerMock.ReturnBook mock is already set by Expect")
	}

	if mmReturnBook.defaultExpectation.paramPtrs == nil {
		mmReturnBook.defaultExpectation.paramPtrs = &RentedBooksManagerMockReturnBookParamPtrs{}
	}
	mmReturnBook.defaultExpectation.paramPtrs.userID = &userID

	return mmReturnBook
}

// ExpectBookIDParam3 sets up expected param bookID for rentedBooksManager.ReturnBook
func (mmReturnBook *mRentedBooksManagerMockReturnBook) ExpectBookIDParam3(bookID int) *mRentedBooksManagerMockReturnBook {
	if mmReturnBook.mock.funcReturnBook != nil {
		mmReturnBook.mock.t.Fatalf("RentedBooksManagerMock.ReturnBook mock is already set by Set")
	}

	if mmReturnBook.defaultExpectation == nil {
		mmReturnBook.defaultExpectation = &RentedBooksManagerMockReturnBookExpectation{}
	}

	if mmReturnBook.defaultExpectation.params != nil {
		mmReturnBook.mock.t.Fatalf("RentedBooksManagerMock.ReturnBook mock is already set by Expect")
	}

	if mmReturnBook.defaultExpectation.paramPtrs == nil {
		mmReturnBook.defaultExpectation.paramPtrs = &RentedBooksManagerMockReturnBookParamPtrs{}
	}
	mmReturnBook.defaultExpectation.paramPtrs.bookID = &bookID

	return mmReturnBook
}

// Inspect accepts an inspector function that has same arguments as the rentedBooksManager.ReturnBook
func (mmReturnBook *mRentedBooksManagerMockReturnBook) Inspect(f func(ctx context.Context, userID int, bookID int)) *mRentedBooksManagerMockReturnBook {
	if mmReturnBook.mock.inspectFuncReturnBook != nil {
		mmReturnBook.mock.t.Fatalf("Inspect function is already set for RentedBooksManagerMock.ReturnBook")
	}

	mmReturnBook.mock.inspectFuncReturnBook = f

	return mmReturnBook
}

// Return sets up results that will be returned by rentedBooksManager.ReturnBook
func (mmReturnBook *mRentedBooksManagerMockReturnBook) Return(i1 int, err error) *RentedBooksManagerMock {
	if mmReturnBook.mock.funcReturnBook != nil {
		mmReturnBook.mock.t.Fatalf("RentedBooksManagerMock.ReturnBook mock is already set by Set")
	}

	if mmReturnBook.defaultExpectation == nil {
		mmReturnBook.defaultExpectation = &RentedBooksManagerMockReturnBookExpectation{mock: mmReturnBook.mock}
	}
	mmReturnBook.defaultExpectation.results = &RentedBooksManagerMockReturnBookResults{i1, err}
	return mmReturnBook.mock
}

// Set uses given function f to mock the rentedBooksManager.ReturnBook method
func (mmReturnBook *mRentedBooksManagerMockReturnBook) Set(f func(ctx context.Context, userID int, bookID int) (i1 int, err error)) *RentedBooksManagerMock {
	if mmReturnBook.defaultExpectation != nil {
		mmReturnBook.mock.t.Fatalf("Default expectation is already set for the rentedBooksManager.ReturnBook method")
	}

	if len(mmReturnBook.expectations) > 0 {
		mmReturnBook.mock.t.Fatalf("Some expectations are already set for the rentedBooksManager.ReturnBook method")
	}

	mmReturnBook.mock.funcReturnBook = f
	return mmReturnBook.mock
}

// When sets expectation for the rentedBooksManager.ReturnBook which will trigger the result defined by the following
// Then helper
func (mmReturnBook *mRentedBooksManagerMockReturnBook) When(ctx context.Context, userID int, bookID int) *RentedBooksManagerMockReturnBookExpectation {
	if mmReturnBook.mock.funcReturnBook != nil {
		mmReturnBook.mock.t.Fatalf("RentedBooksManagerMock.ReturnBook mock is already set by Set")
	}

	expectation := &RentedBooksManagerMockReturnBookExpectation{
		mock:   mmReturnBook.mock,
		params: &RentedBooksManagerMockReturnBookParams{ctx, userID, bookID},
	}
	mmReturnBook.expectations = append(mmReturnBook.expectations, expectation)
	return expectation
}

// Then sets up rentedBooksManager.ReturnBook return parameters for the expectation previously defined by the When method
func (e *RentedBooksManagerMockReturnBookExpectation) Then(i1 int, err error) *RentedBooksManagerMock {
	e.results = &RentedBooksManagerMockReturnBookResults{i1, err}
	return e.mock
}

// Times sets number of times rentedBooksManager.ReturnBook should be invoked
func (mmReturnBook *mRentedBooksManagerMockReturnBook) Times(n uint64) *mRentedBooksManagerMockReturnBook {
	if n == 0 {
		mmReturnBook.mock.t.Fatalf("Times of RentedBooksManagerMock.ReturnBook mock can not be zero")
	}
	mm_atomic.StoreUint64(&mmReturnBook.expectedInvocations, n)
	return mmReturnBook
}

func (mmReturnBook *mRentedBooksManagerMockReturnBook) invocationsDone() bool {
	if len(mmReturnBook.expectations) == 0 && mmReturnBook.defaultExpectation == nil && mmReturnBook.mock.funcReturnBook == nil {
		return true
	}

	totalInvocations := mm_atomic.LoadUint64(&mmReturnBook.mock.afterReturnBookCounter)
	expectedInvocations := mm_atomic.LoadUint64(&mmReturnBook.expectedInvocations)

	return totalInvocations > 0 && (expectedInvocations == 0 || expectedInvocations == totalInvocations)
}

// ReturnBook implements rentedBooksManager
func (mmReturnBook *RentedBooksManagerMock) ReturnBook(ctx context.Context, userID int, bookID int) (i1 int, err error) {
	mm_atomic.AddUint64(&mmReturnBook.beforeReturnBookCounter, 1)
	defer mm_atomic.AddUint64(&mmReturnBook.afterReturnBookCounter, 1)

	if mmReturnBook.inspectFuncReturnBook != nil {
		mmReturnBook.inspectFuncReturnBook(ctx, userID, bookID)
	}

	mm_params := RentedBooksManagerMockReturnBookParams{ctx, userID, bookID}

	// Record call args
	mmReturnBook.ReturnBookMock.mutex.Lock()
	mmReturnBook.ReturnBookMock.callArgs = append(mmReturnBook.ReturnBookMock.callArgs, &mm_params)
	mmReturnBook.ReturnBookMock.mutex.Unlock()

	for _, e := range mmReturnBook.ReturnBookMock.expectations {
		if minimock.Equal(*e.params, mm_params) {
			mm_atomic.AddUint64(&e.Counter, 1)
			return e.results.i1, e.results.err
		}
	}

	if mmReturnBook.ReturnBookMock.defaultExpectation != nil {
		mm_atomic.AddUint64(&mmReturnBook.ReturnBookMock.defaultExpectation.Counter, 1)
		mm_want := mmReturnBook.ReturnBookMock.defaultExpectation.params
		mm_want_ptrs := mmReturnBook.ReturnBookMock.defaultExpectation.paramPtrs

		mm_got := RentedBooksManagerMockReturnBookParams{ctx, userID, bookID}

		if mm_want_ptrs != nil {

			if mm_want_ptrs.ctx != nil && !minimock.Equal(*mm_want_ptrs.ctx, mm_got.ctx) {
				mmReturnBook.t.Errorf("RentedBooksManagerMock.ReturnBook got unexpected parameter ctx, want: %#v, got: %#v%s\n", *mm_want_ptrs.ctx, mm_got.ctx, minimock.Diff(*mm_want_ptrs.ctx, mm_got.ctx))
			}

			if mm_want_ptrs.userID != nil && !minimock.Equal(*mm_want_ptrs.userID, mm_got.userID) {
				mmReturnBook.t.Errorf("RentedBooksManagerMock.ReturnBook got unexpected parameter userID, want: %#v, got: %#v%s\n", *mm_want_ptrs.userID, mm_got.userID, minimock.Diff(*mm_want_ptrs.userID, mm_got.userID))
			}

			if mm_want_ptrs.bookID != nil && !minimock.Equal(*mm_want_ptrs.bookID, mm_got.bookID) {
				mmReturnBook.t.Errorf("RentedBooksManagerMock.ReturnBook got unexpected parameter bookID, want: %#v, got: %#v%s\n", *mm_want_ptrs.bookID, mm_got.bookID, minimock.Diff(*mm_want_ptrs.bookID, mm_got.bookID))
			}

		} else if mm_want != nil && !minimock.Equal(*mm_want, mm_got) {
			mmReturnBook.t.Errorf("RentedBooksManagerMock.ReturnBook got unexpected parameters, want: %#v, got: %#v%s\n", *mm_want, mm_got, minimock.Diff(*mm_want, mm_got))
		}

		mm_results := mmReturnBook.ReturnBookMock.defaultExpectation.results
		if mm_results == nil {
			mmReturnBook.t.Fatal("No results are set for the RentedBooksManagerMock.ReturnBook")
		}
		return (*mm_results).i1, (*mm_results).err
	}
	if mmReturnBook.funcReturnBook != nil {
		return mmReturnBook.funcReturnBook(ctx, userID, bookID)
	}
	mmReturnBook.t.Fatalf("Unexpected call to RentedBooksManagerMock.ReturnBook. %v %v %v", ctx, userID, bookID)
	return
}

// ReturnBookAfterCounter returns a count of finished RentedBooksManagerMock.ReturnBook invocations
func (mmReturnBook *RentedBooksManagerMock) ReturnBookAfterCounter() uint64 {
	return mm_atomic.LoadUint64(&mmReturnBook.afterReturnBookCounter)
}

// ReturnBookBeforeCounter returns a count of RentedBooksManagerMock.ReturnBook invocations
func (mmReturnBook *RentedBooksManagerMock) ReturnBookBeforeCounter() uint64 {
	return mm_atomic.LoadUint64(&mmReturnBook.beforeReturnBookCounter)
}

// Calls returns a list of arguments used in each call to RentedBooksManagerMock.ReturnBook.
// The list is in the same order as the calls were made (i.e. recent calls have a higher index)
func (mmReturnBook *mRentedBooksManagerMockReturnBook) Calls() []*RentedBooksManagerMockReturnBookParams {
	mmReturnBook.mutex.RLock()

	argCopy := make([]*RentedBooksManagerMockReturnBookParams, len(mmReturnBook.callArgs))
	copy(argCopy, mmReturnBook.callArgs)

	mmReturnBook.mutex.RUnlock()

	return argCopy
}

// MinimockReturnBookDone returns true if the count of the ReturnBook invocations corresponds
// the number of defined expectations
func (m *RentedBooksManagerMock) MinimockReturnBookDone() bool {
	if m.ReturnBookMock.optional {
		// Optional methods provide '0 or more' call count restriction.
		return true
	}

	for _, e := range m.ReturnBookMock.expectations {
		if mm_atomic.LoadUint64(&e.Counter) < 1 {
			return false
		}
	}

	return m.ReturnBookMock.invocationsDone()
}

// MinimockReturnBookInspect logs each unmet expectation
func (m *RentedBooksManagerMock) MinimockReturnBookInspect() {
	for _, e := range m.ReturnBookMock.expectations {
		if mm_atomic.LoadUint64(&e.Counter) < 1 {
			m.t.Errorf("Expected call to RentedBooksManagerMock.ReturnBook with params: %#v", *e.params)
		}
	}

	afterReturnBookCounter := mm_atomic.LoadUint64(&m.afterReturnBookCounter)
	// if default expectation was set then invocations count should be greater than zero
	if m.ReturnBookMock.defaultExpectation != nil && afterReturnBookCounter < 1 {
		if m.ReturnBookMock.defaultExpectation.params == nil {
			m.t.Error("Expected call to RentedBooksManagerMock.ReturnBook")
		} else {
			m.t.Errorf("Expected call to RentedBooksManagerMock.ReturnBook with params: %#v", *m.ReturnBookMock.defaultExpectation.params)
		}
	}
	// if func was set then invocations count should be greater than zero
	if m.funcReturnBook != nil && afterReturnBookCounter < 1 {
		m.t.Error("Expected call to RentedBooksManagerMock.ReturnBook")
	}

	if !m.ReturnBookMock.invocationsDone() && afterReturnBookCounter > 0 {
		m.t.Errorf("Expected %d calls to RentedBooksManagerMock.ReturnBook but found %d calls",
			mm_atomic.LoadUint64(&m.ReturnBookMock.expectedInvocations), afterReturnBookCounter)
	}
}

type mRentedBooksManagerMockTakeBook struct {
	optional           bool
	mock               *RentedBooksManagerMock
	defaultExpectation *RentedBooksManagerMockTakeBookExpectation
	expectations       []*RentedBooksManagerMockTakeBookExpectation

	callArgs []*RentedBooksManagerMockTakeBookParams
	mutex    sync.RWMutex

	expectedInvocations uint64
}

// RentedBooksManagerMockTakeBookExpectation specifies expectation struct of the rentedBooksManager.TakeBook
type RentedBooksManagerMockTakeBookExpectation struct {
	mock      *RentedBooksManagerMock
	params    *RentedBooksManagerMockTakeBookParams
	paramPtrs *RentedBooksManagerMockTakeBookParamPtrs
	results   *RentedBooksManagerMockTakeBookResults
	Counter   uint64
}

// RentedBooksManagerMockTakeBookParams contains parameters of the rentedBooksManager.TakeBook
type RentedBooksManagerMockTakeBookParams struct {
	ctx    context.Context
	userID int
	bookID int
}

// RentedBooksManagerMockTakeBookParamPtrs contains pointers to parameters of the rentedBooksManager.TakeBook
type RentedBooksManagerMockTakeBookParamPtrs struct {
	ctx    *context.Context
	userID *int
	bookID *int
}

// RentedBooksManagerMockTakeBookResults contains results of the rentedBooksManager.TakeBook
type RentedBooksManagerMockTakeBookResults struct {
	i1  int
	err error
}

// Marks this method to be optional. The default behavior of any method with Return() is '1 or more', meaning
// the test will fail minimock's automatic final call check if the mocked method was not called at least once.
// Optional() makes method check to work in '0 or more' mode.
// It is NOT RECOMMENDED to use this option unless you really need it, as default behaviour helps to
// catch the problems when the expected method call is totally skipped during test run.
func (mmTakeBook *mRentedBooksManagerMockTakeBook) Optional() *mRentedBooksManagerMockTakeBook {
	mmTakeBook.optional = true
	return mmTakeBook
}

// Expect sets up expected params for rentedBooksManager.TakeBook
func (mmTakeBook *mRentedBooksManagerMockTakeBook) Expect(ctx context.Context, userID int, bookID int) *mRentedBooksManagerMockTakeBook {
	if mmTakeBook.mock.funcTakeBook != nil {
		mmTakeBook.mock.t.Fatalf("RentedBooksManagerMock.TakeBook mock is already set by Set")
	}

	if mmTakeBook.defaultExpectation == nil {
		mmTakeBook.defaultExpectation = &RentedBooksManagerMockTakeBookExpectation{}
	}

	if mmTakeBook.defaultExpectation.paramPtrs != nil {
		mmTakeBook.mock.t.Fatalf("RentedBooksManagerMock.TakeBook mock is already set by ExpectParams functions")
	}

	mmTakeBook.defaultExpectation.params = &RentedBooksManagerMockTakeBookParams{ctx, userID, bookID}
	for _, e := range mmTakeBook.expectations {
		if minimock.Equal(e.params, mmTakeBook.defaultExpectation.params) {
			mmTakeBook.mock.t.Fatalf("Expectation set by When has same params: %#v", *mmTakeBook.defaultExpectation.params)
		}
	}

	return mmTakeBook
}

// ExpectCtxParam1 sets up expected param ctx for rentedBooksManager.TakeBook
func (mmTakeBook *mRentedBooksManagerMockTakeBook) ExpectCtxParam1(ctx context.Context) *mRentedBooksManagerMockTakeBook {
	if mmTakeBook.mock.funcTakeBook != nil {
		mmTakeBook.mock.t.Fatalf("RentedBooksManagerMock.TakeBook mock is already set by Set")
	}

	if mmTakeBook.defaultExpectation == nil {
		mmTakeBook.defaultExpectation = &RentedBooksManagerMockTakeBookExpectation{}
	}

	if mmTakeBook.defaultExpectation.params != nil {
		mmTakeBook.mock.t.Fatalf("RentedBooksManagerMock.TakeBook mock is already set by Expect")
	}

	if mmTakeBook.defaultExpectation.paramPtrs == nil {
		mmTakeBook.defaultExpectation.paramPtrs = &RentedBooksManagerMockTakeBookParamPtrs{}
	}
	mmTakeBook.defaultExpectation.paramPtrs.ctx = &ctx

	return mmTakeBook
}

// ExpectUserIDParam2 sets up expected param userID for rentedBooksManager.TakeBook
func (mmTakeBook *mRentedBooksManagerMockTakeBook) ExpectUserIDParam2(userID int) *mRentedBooksManagerMockTakeBook {
	if mmTakeBook.mock.funcTakeBook != nil {
		mmTakeBook.mock.t.Fatalf("RentedBooksManagerMock.TakeBook mock is already set by Set")
	}

	if mmTakeBook.defaultExpectation == nil {
		mmTakeBook.defaultExpectation = &RentedBooksManagerMockTakeBookExpectation{}
	}

	if mmTakeBook.defaultExpectation.params != nil {
		mmTakeBook.mock.t.Fatalf("RentedBooksManagerMock.TakeBook mock is already set by Expect")
	}

	if mmTakeBook.defaultExpectation.paramPtrs == nil {
		mmTakeBook.defaultExpectation.paramPtrs = &RentedBooksManagerMockTakeBookParamPtrs{}
	}
	mmTakeBook.defaultExpectation.paramPtrs.userID = &userID

	return mmTakeBook
}

// ExpectBookIDParam3 sets up expected param bookID for rentedBooksManager.TakeBook
func (mmTakeBook *mRentedBooksManagerMockTakeBook) ExpectBookIDParam3(bookID int) *mRentedBooksManagerMockTakeBook {
	if mmTakeBook.mock.funcTakeBook != nil {
		mmTakeBook.mock.t.Fatalf("RentedBooksManagerMock.TakeBook mock is already set by Set")
	}

	if mmTakeBook.defaultExpectation == nil {
		mmTakeBook.defaultExpectation = &RentedBooksManagerMockTakeBookExpectation{}
	}

	if mmTakeBook.defaultExpectation.params != nil {
		mmTakeBook.mock.t.Fatalf("RentedBooksManagerMock.TakeBook mock is already set by Expect")
	}

	if mmTakeBook.defaultExpectation.paramPtrs == nil {
		mmTakeBook.defaultExpectation.paramPtrs = &RentedBooksManagerMockTakeBookParamPtrs{}
	}
	mmTakeBook.defaultExpectation.paramPtrs.bookID = &bookID

	return mmTakeBook
}

// Inspect accepts an inspector function that has same arguments as the rentedBooksManager.TakeBook
func (mmTakeBook *mRentedBooksManagerMockTakeBook) Inspect(f func(ctx context.Context, userID int, bookID int)) *mRentedBooksManagerMockTakeBook {
	if mmTakeBook.mock.inspectFuncTakeBook != nil {
		mmTakeBook.mock.t.Fatalf("Inspect function is already set for RentedBooksManagerMock.TakeBook")
	}

	mmTakeBook.mock.inspectFuncTakeBook = f

	return mmTakeBook
}

// Return sets up results that will be returned by rentedBooksManager.TakeBook
func (mmTakeBook *mRentedBooksManagerMockTakeBook) Return(i1 int, err error) *RentedBooksManagerMock {
	if mmTakeBook.mock.funcTakeBook != nil {
		mmTakeBook.mock.t.Fatalf("RentedBooksManagerMock.TakeBook mock is already set by Set")
	}

	if mmTakeBook.defaultExpectation == nil {
		mmTakeBook.defaultExpectation = &RentedBooksManagerMockTakeBookExpectation{mock: mmTakeBook.mock}
	}
	mmTakeBook.defaultExpectation.results = &RentedBooksManagerMockTakeBookResults{i1, err}
	return mmTakeBook.mock
}

// Set uses given function f to mock the rentedBooksManager.TakeBook method
func (mmTakeBook *mRentedBooksManagerMockTakeBook) Set(f func(ctx context.Context, userID int, bookID int) (i1 int, err error)) *RentedBooksManagerMock {
	if mmTakeBook.defaultExpectation != nil {
		mmTakeBook.mock.t.Fatalf("Default expectation is already set for the rentedBooksManager.TakeBook method")
	}

	if len(mmTakeBook.expectations) > 0 {
		mmTakeBook.mock.t.Fatalf("Some expectations are already set for the rentedBooksManager.TakeBook method")
	}

	mmTakeBook.mock.funcTakeBook = f
	return mmTakeBook.mock
}

// When sets expectation for the rentedBooksManager.TakeBook which will trigger the result defined by the following
// Then helper
func (mmTakeBook *mRentedBooksManagerMockTakeBook) When(ctx context.Context, userID int, bookID int) *RentedBooksManagerMockTakeBookExpectation {
	if mmTakeBook.mock.funcTakeBook != nil {
		mmTakeBook.mock.t.Fatalf("RentedBooksManagerMock.TakeBook mock is already set by Set")
	}

	expectation := &RentedBooksManagerMockTakeBookExpectation{
		mock:   mmTakeBook.mock,
		params: &RentedBooksManagerMockTakeBookParams{ctx, userID, bookID},
	}
	mmTakeBook.expectations = append(mmTakeBook.expectations, expectation)
	return expectation
}

// Then sets up rentedBooksManager.TakeBook return parameters for the expectation previously defined by the When method
func (e *RentedBooksManagerMockTakeBookExpectation) Then(i1 int, err error) *RentedBooksManagerMock {
	e.results = &RentedBooksManagerMockTakeBookResults{i1, err}
	return e.mock
}

// Times sets number of times rentedBooksManager.TakeBook should be invoked
func (mmTakeBook *mRentedBooksManagerMockTakeBook) Times(n uint64) *mRentedBooksManagerMockTakeBook {
	if n == 0 {
		mmTakeBook.mock.t.Fatalf("Times of RentedBooksManagerMock.TakeBook mock can not be zero")
	}
	mm_atomic.StoreUint64(&mmTakeBook.expectedInvocations, n)
	return mmTakeBook
}

func (mmTakeBook *mRentedBooksManagerMockTakeBook) invocationsDone() bool {
	if len(mmTakeBook.expectations) == 0 && mmTakeBook.defaultExpectation == nil && mmTakeBook.mock.funcTakeBook == nil {
		return true
	}

	totalInvocations := mm_atomic.LoadUint64(&mmTakeBook.mock.afterTakeBookCounter)
	expectedInvocations := mm_atomic.LoadUint64(&mmTakeBook.expectedInvocations)

	return totalInvocations > 0 && (expectedInvocations == 0 || expectedInvocations == totalInvocations)
}

// TakeBook implements rentedBooksManager
func (mmTakeBook *RentedBooksManagerMock) TakeBook(ctx context.Context, userID int, bookID int) (i1 int, err error) {
	mm_atomic.AddUint64(&mmTakeBook.beforeTakeBookCounter, 1)
	defer mm_atomic.AddUint64(&mmTakeBook.afterTakeBookCounter, 1)

	if mmTakeBook.inspectFuncTakeBook != nil {
		mmTakeBook.inspectFuncTakeBook(ctx, userID, bookID)
	}

	mm_params := RentedBooksManagerMockTakeBookParams{ctx, userID, bookID}

	// Record call args
	mmTakeBook.TakeBookMock.mutex.Lock()
	mmTakeBook.TakeBookMock.callArgs = append(mmTakeBook.TakeBookMock.callArgs, &mm_params)
	mmTakeBook.TakeBookMock.mutex.Unlock()

	for _, e := range mmTakeBook.TakeBookMock.expectations {
		if minimock.Equal(*e.params, mm_params) {
			mm_atomic.AddUint64(&e.Counter, 1)
			return e.results.i1, e.results.err
		}
	}

	if mmTakeBook.TakeBookMock.defaultExpectation != nil {
		mm_atomic.AddUint64(&mmTakeBook.TakeBookMock.defaultExpectation.Counter, 1)
		mm_want := mmTakeBook.TakeBookMock.defaultExpectation.params
		mm_want_ptrs := mmTakeBook.TakeBookMock.defaultExpectation.paramPtrs

		mm_got := RentedBooksManagerMockTakeBookParams{ctx, userID, bookID}

		if mm_want_ptrs != nil {

			if mm_want_ptrs.ctx != nil && !minimock.Equal(*mm_want_ptrs.ctx, mm_got.ctx) {
				mmTakeBook.t.Errorf("RentedBooksManagerMock.TakeBook got unexpected parameter ctx, want: %#v, got: %#v%s\n", *mm_want_ptrs.ctx, mm_got.ctx, minimock.Diff(*mm_want_ptrs.ctx, mm_got.ctx))
			}

			if mm_want_ptrs.userID != nil && !minimock.Equal(*mm_want_ptrs.userID, mm_got.userID) {
				mmTakeBook.t.Errorf("RentedBooksManagerMock.TakeBook got unexpected parameter userID, want: %#v, got: %#v%s\n", *mm_want_ptrs.userID, mm_got.userID, minimock.Diff(*mm_want_ptrs.userID, mm_got.userID))
			}

			if mm_want_ptrs.bookID != nil && !minimock.Equal(*mm_want_ptrs.bookID, mm_got.bookID) {
				mmTakeBook.t.Errorf("RentedBooksManagerMock.TakeBook got unexpected parameter bookID, want: %#v, got: %#v%s\n", *mm_want_ptrs.bookID, mm_got.bookID, minimock.Diff(*mm_want_ptrs.bookID, mm_got.bookID))
			}

		} else if mm_want != nil && !minimock.Equal(*mm_want, mm_got) {
			mmTakeBook.t.Errorf("RentedBooksManagerMock.TakeBook got unexpected parameters, want: %#v, got: %#v%s\n", *mm_want, mm_got, minimock.Diff(*mm_want, mm_got))
		}

		mm_results := mmTakeBook.TakeBookMock.defaultExpectation.results
		if mm_results == nil {
			mmTakeBook.t.Fatal("No results are set for the RentedBooksManagerMock.TakeBook")
		}
		return (*mm_results).i1, (*mm_results).err
	}
	if mmTakeBook.funcTakeBook != nil {
		return mmTakeBook.funcTakeBook(ctx, userID, bookID)
	}
	mmTakeBook.t.Fatalf("Unexpected call to RentedBooksManagerMock.TakeBook. %v %v %v", ctx, userID, bookID)
	return
}

// TakeBookAfterCounter returns a count of finished RentedBooksManagerMock.TakeBook invocations
func (mmTakeBook *RentedBooksManagerMock) TakeBookAfterCounter() uint64 {
	return mm_atomic.LoadUint64(&mmTakeBook.afterTakeBookCounter)
}

// TakeBookBeforeCounter returns a count of RentedBooksManagerMock.TakeBook invocations
func (mmTakeBook *RentedBooksManagerMock) TakeBookBeforeCounter() uint64 {
	return mm_atomic.LoadUint64(&mmTakeBook.beforeTakeBookCounter)
}

// Calls returns a list of arguments used in each call to RentedBooksManagerMock.TakeBook.
// The list is in the same order as the calls were made (i.e. recent calls have a higher index)
func (mmTakeBook *mRentedBooksManagerMockTakeBook) Calls() []*RentedBooksManagerMockTakeBookParams {
	mmTakeBook.mutex.RLock()

	argCopy := make([]*RentedBooksManagerMockTakeBookParams, len(mmTakeBook.callArgs))
	copy(argCopy, mmTakeBook.callArgs)

	mmTakeBook.mutex.RUnlock()

	return argCopy
}

// MinimockTakeBookDone returns true if the count of the TakeBook invocations corresponds
// the number of defined expectations
func (m *RentedBooksManagerMock) MinimockTakeBookDone() bool {
	if m.TakeBookMock.optional {
		// Optional methods provide '0 or more' call count restriction.
		return true
	}

	for _, e := range m.TakeBookMock.expectations {
		if mm_atomic.LoadUint64(&e.Counter) < 1 {
			return false
		}
	}

	return m.TakeBookMock.invocationsDone()
}

// MinimockTakeBookInspect logs each unmet expectation
func (m *RentedBooksManagerMock) MinimockTakeBookInspect() {
	for _, e := range m.TakeBookMock.expectations {
		if mm_atomic.LoadUint64(&e.Counter) < 1 {
			m.t.Errorf("Expected call to RentedBooksManagerMock.TakeBook with params: %#v", *e.params)
		}
	}

	afterTakeBookCounter := mm_atomic.LoadUint64(&m.afterTakeBookCounter)
	// if default expectation was set then invocations count should be greater than zero
	if m.TakeBookMock.defaultExpectation != nil && afterTakeBookCounter < 1 {
		if m.TakeBookMock.defaultExpectation.params == nil {
			m.t.Error("Expected call to RentedBooksManagerMock.TakeBook")
		} else {
			m.t.Errorf("Expected call to RentedBooksManagerMock.TakeBook with params: %#v", *m.TakeBookMock.defaultExpectation.params)
		}
	}
	// if func was set then invocations count should be greater than zero
	if m.funcTakeBook != nil && afterTakeBookCounter < 1 {
		m.t.Error("Expected call to RentedBooksManagerMock.TakeBook")
	}

	if !m.TakeBookMock.invocationsDone() && afterTakeBookCounter > 0 {
		m.t.Errorf("Expected %d calls to RentedBooksManagerMock.TakeBook but found %d calls",
			mm_atomic.LoadUint64(&m.TakeBookMock.expectedInvocations), afterTakeBookCounter)
	}
}

// MinimockFinish checks that all mocked methods have been called the expected number of times
func (m *RentedBooksManagerMock) MinimockFinish() {
	m.finishOnce.Do(func() {
		if !m.minimockDone() {
			m.MinimockReturnBookInspect()

			m.MinimockTakeBookInspect()
		}
	})
}

// MinimockWait waits for all mocked methods to be called the expected number of times
func (m *RentedBooksManagerMock) MinimockWait(timeout mm_time.Duration) {
	timeoutCh := mm_time.After(timeout)
	for {
		if m.minimockDone() {
			return
		}
		select {
		case <-timeoutCh:
			m.MinimockFinish()
			return
		case <-mm_time.After(10 * mm_time.Millisecond):
		}
	}
}

func (m *RentedBooksManagerMock) minimockDone() bool {
	done := true
	return done &&
		m.MinimockReturnBookDone() &&
		m.MinimockTakeBookDone()
}
