package author

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	"library/app/internal/models/controller/author"
	error_models "library/app/internal/models/errors_domain"
	"net/http/httptest"
	"testing"
)

func TestHandler_AddAuthor(t *testing.T) {
	const (
		method = "POST"

		url = "/authors"

		authorID         = 2
		authorFirstName  = "Антон"
		authorMiddleName = "Павлович"
		authorLastName   = "Чехов"
	)

	var (
		req = author.AddAuthorRequest{
			FirstName:  authorFirstName,
			MiddleName: authorMiddleName,
			LastName:   authorLastName,
		}

		resp = author.AddAuthorResponse{
			AuthorID: authorID,
		}
	)

	reqBody, _ := json.Marshal(&req)
	respBody, _ := json.Marshal(&resp)

	tests := []struct {
		name     string
		setup    func(ctx context.Context, cntrl *minimock.Controller) Handler
		reqBody  []byte
		wantBody []byte
	}{
		{name: "ok",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewAuthorsManagerMock(cntrl)
				mock.AddAuthorMock.Expect(ctx, req.ToModel()).Return(authorID, nil)

				return Handler{
					authorService: mock,
				}
			},
			reqBody:  reqBody,
			wantBody: respBody,
		},

		{
			name: "with err in user manager",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewAuthorsManagerMock(cntrl)
				mock.AddAuthorMock.Expect(ctx, req.ToModel()).Return(0, error_models.ErrAuthorNotFound)

				return Handler{
					authorService: mock,
				}
			},
			reqBody:  reqBody,
			wantBody: nil,
		},

		{name: "with err unmarshal",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewAuthorsManagerMock(cntrl)

				return Handler{
					authorService: mock,
				}
			},
			reqBody:  nil,
			wantBody: nil,
		},
		// TODO: Add test cases.
	}

	cntrl := minimock.NewController(t)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testRequest := httptest.NewRequest(method, url, bytes.NewReader(tt.reqBody))
			response := httptest.NewRecorder()

			handler := tt.setup(testRequest.Context(), cntrl)

			handler.AddAuthor(response, testRequest)

			assert.Equal(t, tt.wantBody, response.Body.Bytes())
		})
	}
}
