package author

import (
	"encoding/json"
	"library/app/internal/lib/context_value"
	"net/http"
	"net/url"
	"strconv"
)

// Add Author godoc
// @Summary      Get list of authors
// @Description  You can find author and him books by author-id. Empty query - get all authors.
// @Tags         authors
// @Accept       json
// @Produce      json
// @Param        author-id    query     string  false  "author's ID"  Format(author-id)
// @Success		200		{array}	author.GetAuthorsResponse
// @router       /authors [GET]
func (a *Handler) GetAuthors(w http.ResponseWriter, r *http.Request) {
	const paramKey = "author-id"
	var authorID int

	parseURL, err := url.Parse(r.URL.String())
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	params, err := url.ParseQuery(parseURL.RawQuery)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)
		return
	}

	authorID, err = strconv.Atoi(params.Get(paramKey))
	if err != nil && params.Get(paramKey) != "" {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	authors, err := a.authorService.ShowAuthors(r.Context(), authorID)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	authorsResponse := AuthorsToResponse(authors)
	response, err := json.Marshal(&authorsResponse)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	w.Write(response)
}
