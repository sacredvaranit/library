package author

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	"library/app/internal/models/domain/author"
	"library/app/internal/models/domain/author_book"
	"library/app/internal/models/domain/book"
	error_models "library/app/internal/models/errors_domain"
	"net/http/httptest"
	"testing"
)

func TestHandler_GetAuthors(t *testing.T) {
	const (
		method = "GET"

		url = "/books?author-id=%s"

		paramAuthorID      = "2"
		wrongParamAuthorID = "asdsa"

		firstBookID  = 5
		secondBookID = 6

		firstBookName  = "5"
		secondBookName = "6"

		authorID         = 2
		authorFirstName  = "Антон"
		authorMiddleName = "Павлович"
		authorLastName   = "Чехов"
	)

	var (
		authorOfBooks = author.Author{
			ID:         authorID,
			FirstName:  authorFirstName,
			MiddleName: authorMiddleName,
			LastName:   authorLastName,
		}

		authors = []author_book.AuthorBooksAggregator{
			{
				Author: authorOfBooks,
				Books: []book.Book{
					{
						ID:       firstBookID,
						Name:     firstBookName,
						AuthorID: authorID,
						IsIssued: false,
					},

					{
						ID:       secondBookID,
						Name:     secondBookName,
						AuthorID: authorID,
						IsIssued: false,
					},
				},
			},
		}

		resp = AuthorsToResponse(authors)
	)

	respBody, _ := json.Marshal(&resp)

	tests := []struct {
		name     string
		setup    func(ctx context.Context, cntrl *minimock.Controller) Handler
		url      string
		wantBody []byte
	}{
		{name: "ok",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewAuthorsManagerMock(cntrl)
				mock.ShowAuthorsMock.Expect(ctx, authorID).Return(authors, nil)

				return Handler{
					authorService: mock,
				}
			},
			url:      fmt.Sprintf(url, paramAuthorID),
			wantBody: respBody,
		},

		{
			name: "with err in user manager",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewAuthorsManagerMock(cntrl)
				mock.ShowAuthorsMock.Expect(ctx, authorID).Return(authors, error_models.ErrAuthorNotFound)

				return Handler{
					authorService: mock,
				}
			},
			url:      fmt.Sprintf(url, paramAuthorID),
			wantBody: nil,
		},

		{name: "with err unmarshal",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewAuthorsManagerMock(cntrl)

				return Handler{
					authorService: mock,
				}
			},
			url:      fmt.Sprintf(url, wrongParamAuthorID),
			wantBody: nil,
		},
		// TODO: Add test cases.
	}

	cntrl := minimock.NewController(t)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testRequest := httptest.NewRequest(method, tt.url, nil)
			response := httptest.NewRecorder()

			handler := tt.setup(testRequest.Context(), cntrl)

			handler.GetAuthors(response, testRequest)

			assert.Equal(t, tt.wantBody, response.Body.Bytes())
		})
	}
}
