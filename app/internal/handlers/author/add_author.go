package author

import (
	"encoding/json"
	"library/app/internal/lib/context_value"
	"library/app/internal/lib/validate"
	"library/app/internal/models/controller/author"
	"net/http"
)

// Add Author godoc
// @Summary      Add author
// @Description  You can add new author into table
// @Tags         authors
// @Accept       json
// @Produce      json
// @Param        input 	body 	author.AddAuthorRequest 	true 	"Added author"
// @Success		200		{object}	author.AddAuthorResponse
// @router       /authors [POST]
func (h *Handler) AddAuthor(w http.ResponseWriter, r *http.Request) {
	request, err := DecodeAddAuthor(r.Body)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	if err = validate.ValidateRequest(request); err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	authorID, err := h.authorService.AddAuthor(r.Context(), request.ToModel())
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	response, err := json.Marshal(author.AddAuthorResponse{AuthorID: authorID})
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	w.Write(response)
}
