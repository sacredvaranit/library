package author

import (
	"context"
	"net/http"

	"library/app/internal/models/domain/author"
	"library/app/internal/models/domain/author_book"
)

type authorsManager interface {
	AddAuthor(ctx context.Context, author author.Author) (int, error)
	ShowAuthors(ctx context.Context, authorID int) ([]author_book.AuthorBooksAggregator, error)
}

type Handler struct {
	authorService authorsManager
}

func NewHandler(authorService authorsManager) *Handler {
	return &Handler{authorService: authorService}
}

type router interface {
	Get(pattern string, handlerFn http.HandlerFunc)
	Post(pattern string, handlerFn http.HandlerFunc)
}

func (h *Handler) Register(r router) {
	r.Post("/authors", h.AddAuthor)
	r.Get("/authors", h.GetAuthors)
}
