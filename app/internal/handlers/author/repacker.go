package author

import (
	"encoding/json"
	"io"

	"library/app/internal/models/controller/author"
	"library/app/internal/models/domain/author_book"
)

func DecodeAddAuthor(r io.Reader) (author.AddAuthorRequest, error) {
	authorRequest := author.AddAuthorRequest{}

	err := json.NewDecoder(r).Decode(&authorRequest)
	if err != nil {
		return author.AddAuthorRequest{}, err
	}

	return authorRequest, nil
}

func AuthorsToResponse(authors []author_book.AuthorBooksAggregator) []author.GetAuthorsResponse {
	responseAuthors := make([]author.GetAuthorsResponse, 0)

	for _, useAuthor := range authors {
		booksAuthor := make([]struct {
			Name string `json:"bookName"`
		}, 0)
		for _, b := range useAuthor.Books {
			booksAuthor = append(booksAuthor, struct {
				Name string `json:"bookName"`
			}{Name: b.Name})
		}

		responseAuthors = append(
			responseAuthors,
			author.GetAuthorsResponse{
				ID:         useAuthor.Author.ID,
				FirstName:  useAuthor.Author.FirstName,
				MiddleName: useAuthor.Author.MiddleName,
				LastName:   useAuthor.Author.LastName,
				Books:      booksAuthor,
			},
		)
	}

	return responseAuthors
}
