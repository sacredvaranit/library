package swag

import (
	"net/http"

	httpSwagger "github.com/swaggo/http-swagger"

	_ "library/app/api/docs"
)

type SwagHandler struct{}

func NewSwagHandler() *SwagHandler {
	return &SwagHandler{}
}

type router interface {
	Get(pattern string, handlerFn http.HandlerFunc)
	Post(pattern string, handlerFn http.HandlerFunc)
}

func (h *SwagHandler) Register(router router) {
	router.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:8080/swagger/doc.json"), //The url pointing to API definition
	))
}
