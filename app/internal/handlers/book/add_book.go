package book

import (
	"encoding/json"
	"library/app/internal/lib/context_value"
	"library/app/internal/lib/validate"
	"library/app/internal/models/controller/book"
	"net/http"
)

// Add Book godoc
// @Summary      Add book
// @Description  You can add book into table books with author's id.
// @Tags         books
// @Accept       json
// @Produce      json
// @Param        input 	body 	book.AddBookRequest 	true 	"Added book"
// @Success		200		{object}	book.AddBookResponse
// @router       /books [POST]
func (h *Handler) AddBookToList(w http.ResponseWriter, r *http.Request) {
	request, err := decodeForAddBook(r.Body)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	if err = validate.ValidateRequest(request); err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	bookID, err := h.bookServicer.AddBook(r.Context(), request.ToModel())
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	response, err := json.Marshal(book.AddBookResponse{BookID: bookID})
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	w.Write(response)
}
