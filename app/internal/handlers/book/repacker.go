package book

import (
	"encoding/json"
	"io"

	"library/app/internal/models/controller/book"
	"library/app/internal/models/domain/book_author"
)

func decodeForAddBook(r io.Reader) (book.AddBookRequest, error) {
	var bookRequest book.AddBookRequest

	err := json.NewDecoder(r).Decode(&bookRequest)
	if err != nil {
		return book.AddBookRequest{}, err
	}

	return bookRequest, nil
}

func booksToResponse(books []book_author.BookAuthorAggregator) []book.GetBooksResponse {
	responseBooks := make([]book.GetBooksResponse, 0)
	for _, useBook := range books {
		responseBooks = append(responseBooks,
			book.GetBooksResponse{
				ID:   useBook.Book.ID,
				Name: useBook.Book.Name,
				Author: book.AuthorBook{
					FirstName:  useBook.Author.FirstName,
					MiddleName: useBook.Author.MiddleName,
					LastName:   useBook.Author.LastName,
				},
			},
		)
	}

	return responseBooks
}
