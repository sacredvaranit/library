package book

import (
	"context"
	"net/http"

	"library/app/internal/models/domain/book"
	"library/app/internal/models/domain/book_author"
)

type bookManager interface {
	AddBook(ctx context.Context, book book.Book) (int, error)
	ShowBooks(ctx context.Context, authorID int) ([]book_author.BookAuthorAggregator, error)
}

type Handler struct {
	bookServicer bookManager
}

func NewHandler(service bookManager) *Handler {
	return &Handler{bookServicer: service}
}

type router interface {
	Get(pattern string, handlerFn http.HandlerFunc)
	Post(pattern string, handlerFn http.HandlerFunc)
}

func (h *Handler) Register(r router) {
	r.Post("/books", h.AddBookToList)
	r.Get("/books", h.GetBooks)
}
