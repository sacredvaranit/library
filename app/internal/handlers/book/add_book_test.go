package book

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	"library/app/internal/models/controller/book"
	error_models "library/app/internal/models/errors_domain"
	"net/http/httptest"
	"testing"
)

func TestHandler_AddBookToList(t *testing.T) {
	const (
		method = "POST"

		url = "/books"

		id = 1

		authorID = 3
		bookName = "Лес"
	)

	var (
		req = book.AddBookRequest{
			Name:     bookName,
			AuthorID: authorID,
		}

		resp = book.AddBookResponse{
			BookID: id,
		}
	)

	respBody, _ := json.Marshal(&resp)

	reqBody, _ := json.Marshal(&req)

	tests := []struct {
		name     string
		setup    func(ctx context.Context, cntrl *minimock.Controller) Handler
		reqBody  []byte
		wantBody []byte
	}{
		{name: "ok",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewBookManagerMock(cntrl)
				mock.AddBookMock.Expect(ctx, req.ToModel()).Return(id, nil)

				return Handler{
					bookServicer: mock,
				}
			},
			reqBody:  reqBody,
			wantBody: respBody,
		},

		{
			name: "with err in user manager",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewBookManagerMock(cntrl)
				mock.AddBookMock.Expect(ctx, req.ToModel()).Return(0, error_models.ErrBookExist)

				return Handler{
					bookServicer: mock,
				}
			},
			reqBody:  reqBody,
			wantBody: nil,
		},

		{name: "with err unmarshal",
			setup: func(ctx context.Context, cntrl *minimock.Controller) Handler {
				mock := NewBookManagerMock(cntrl)

				return Handler{
					bookServicer: mock,
				}
			},
			reqBody:  nil,
			wantBody: nil,
		},
		// TODO: Add test cases.
	}

	cntrl := minimock.NewController(t)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testRequest := httptest.NewRequest(method, url, bytes.NewReader(tt.reqBody))
			response := httptest.NewRecorder()

			handler := tt.setup(testRequest.Context(), cntrl)

			handler.AddBookToList(response, testRequest)

			assert.Equal(t, tt.wantBody, response.Body.Bytes())
		})
	}
}
