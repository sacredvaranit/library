package book

import (
	"encoding/json"
	"library/app/internal/lib/context_value"
	"net/http"
	"net/url"
	"strconv"
)

// Get Books godoc
// @Summary      Get books
// @Description  You can find books by author-id. Empty query - get all books.
// @Tags         books
// @Accept       json
// @Produce      json
// @Param        author-id    query     int  false  "author's ID"  Format(author-id)
// @Success		200		{array}	book.GetBooksResponse
// @router       /books [GET]
func (h *Handler) GetBooks(w http.ResponseWriter, r *http.Request) {
	const paramKey = "author-id"
	var authorID int

	parseURL, err := url.Parse(r.URL.String())
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	params, err := url.ParseQuery(parseURL.RawQuery)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	authorID, err = strconv.Atoi(params.Get(paramKey))
	if err != nil && params.Get(paramKey) != "" {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	books, err := h.bookServicer.ShowBooks(r.Context(), authorID)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	booksResponse := booksToResponse(books)

	response, err := json.Marshal(&booksResponse)
	if err != nil {
		context_value.WrapErrorToRequestCtx(r, err)

		return
	}

	w.Write(response)
}
