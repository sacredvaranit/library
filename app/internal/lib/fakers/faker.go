package fakers

import (
	"context"
	pgx "github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
)

const (
	usersCount   = 50
	authorsCount = 10
	booksCount   = 100
)

type dbConnector interface {
	Exec(ctx context.Context, sql string, arguments ...any) (pgconn.CommandTag, error)
	Query(ctx context.Context, sql string, args ...any) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, args ...any) pgx.Row
}

type StartUper interface {
	StartCreateUsers(ctx context.Context, count int) error
	StartCreateAuthors(ctx context.Context, count int) error
	StartCreateBooks(ctx context.Context, count int) error
}

type FakesCreator struct {
	db dbConnector
}

func NewFakesCreator(db dbConnector) *FakesCreator {
	return &FakesCreator{db: db}
}

func (f *FakesCreator) StartUp(ctx context.Context) error {
	err := f.StartCreateUsers(ctx, usersCount)
	if err != nil {
		return err
	}

	err = f.StartCreateAuthors(ctx, authorsCount)
	if err != nil {
		return err
	}

	err = f.StartCreateBooks(ctx, booksCount)
	if err != nil {
		return err
	}

	return nil
}
