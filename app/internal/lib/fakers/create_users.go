package fakers

import (
	"context"
	"fmt"
	gofakeit "github.com/brianvoe/gofakeit/v6"
	"strings"
	"time"

	"library/app/internal/models/domain/user"
)

var (
	minDateBitrhday = time.Date(1970, time.January, 1, 0, 0, 0, 0, time.UTC)
	maxDateBitrhday = time.Date(2010, time.December, 31, 0, 0, 0, 0, time.UTC)
)

func (f *FakesCreator) StartCreateUsers(ctx context.Context, count int) error {
	countUsers, err := f.getCountUsers(ctx)
	if err != nil {
		return err
	}

	if countUsers < count {
		users := getMassiveUsers(count - countUsers)

		return f.insertUsers(ctx, users)
	}

	return nil
}

func (f *FakesCreator) getCountUsers(ctx context.Context) (int, error) {
	const sql = `
		SELECT COUNT(u.id) FROM library.users u
`

	var count int

	row := f.db.QueryRow(ctx, sql)
	err := row.Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (f *FakesCreator) insertUsers(ctx context.Context, users []user.User) error {
	sql, values := prepareQueryUsers(users)

	_, err := f.db.Exec(ctx, sql, values...)

	return err
}

func prepareQueryUsers(users []user.User) (string, []interface{}) {
	const count = 4

	var sql strings.Builder

	values := make([]interface{}, 0, count*len(users))

	sql.WriteString("INSERT INTO library.users (first_name, middle_name, last_name, birthday) VALUES ")

	for i, user := range users {
		sql.WriteString(fmt.Sprintf("($%d, $%d, $%d, $%d),",
			count*i+1, count*i+2, count*i+3, count*i+4))

		values = append(values, user.FirstName, user.MiddleName, user.LastName, user.Birthday)
	}

	return sql.String()[:sql.Len()-1], values
}

func getMassiveUsers(num int) []user.User {
	fakeUsers := make([]user.User, 0, num)
	for i := 0; i < num; i++ {
		fakeUsers = append(fakeUsers,
			user.User{
				FirstName:  gofakeit.FirstName(),
				MiddleName: gofakeit.MiddleName(),
				LastName:   gofakeit.LastName(),
				Birthday:   gofakeit.DateRange(minDateBitrhday, maxDateBitrhday),
			})
	}

	return fakeUsers
}
