package fakers

import (
	"context"
	"fmt"
	gofakeit "github.com/brianvoe/gofakeit/v6"
	"strings"

	"library/app/internal/models/domain/author"
)

func (f *FakesCreator) StartCreateAuthors(ctx context.Context, count int) error {
	countAuthors, err := f.getCountAuthors(ctx)
	if err != nil {
		return err
	}

	if countAuthors < count {
		authors := getMassiveAuthors(count - countAuthors)

		return f.insertAuthors(ctx, authors)
	}

	return nil
}

func (f *FakesCreator) getCountAuthors(ctx context.Context) (int, error) {
	const sql = `
		SELECT COUNT(a.id) FROM library.authors a
`

	var count int

	row := f.db.QueryRow(ctx, sql)
	err := row.Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (f *FakesCreator) insertAuthors(ctx context.Context, authors []author.Author) error {
	sql, values := prepareQueryAuthors(authors)

	_, err := f.db.Exec(ctx, sql, values...)

	return err
}

func prepareQueryAuthors(authors []author.Author) (string, []interface{}) {
	const count = 3

	var sql strings.Builder

	values := make([]interface{}, 0, count*len(authors))

	sql.WriteString("INSERT INTO library.authors (first_name, middle_name, last_name) VALUES ")

	for i, author := range authors {
		sql.WriteString(fmt.Sprintf("($%d, $%d, $%d),",
			count*i+1, count*i+2, count*i+3))

		values = append(values, author.FirstName, author.MiddleName, author.LastName)
	}

	return sql.String()[:sql.Len()-1], values
}

func getMassiveAuthors(num int) []author.Author {
	fakeAuthors := make([]author.Author, 0, num)
	for i := 0; i < num; i++ {
		fakeAuthors = append(fakeAuthors,
			author.Author{
				FirstName:  gofakeit.FirstName(),
				MiddleName: gofakeit.MiddleName(),
				LastName:   gofakeit.LastName(),
			})
	}

	return fakeAuthors
}
