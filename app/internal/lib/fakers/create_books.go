package fakers

import (
	"context"
	"fmt"
	gofakeit "github.com/brianvoe/gofakeit/v6"
	"strings"

	"library/app/internal/models/domain/book"
)

func (f *FakesCreator) StartCreateBooks(ctx context.Context, count int) error {
	countBooks, err := f.GetCountBooks(ctx)
	if err != nil {

		return err
	}

	if count > countBooks {
		authorIDs, err := f.GetAuthorIDs(ctx)
		if err != nil {

			return err
		}

		books := getMassiveBooks(count-countBooks, authorIDs)

		err = f.insertBooks(ctx, books)
		if err != nil {
			return err
		}
	}

	return nil
}

func (f *FakesCreator) GetCountBooks(ctx context.Context) (int, error) {
	const sql = `
		SELECT COUNT(b.id) FROM library.books b
`

	var count int

	row := f.db.QueryRow(ctx, sql)

	err := row.Scan(&count)
	if err != nil {

		return 0, nil
	}

	return count, nil
}

func (f *FakesCreator) GetAuthorIDs(ctx context.Context) ([]int, error) {
	const sql = `
		SELECT a.id FROM library.authors a 
`

	rows, err := f.db.Query(ctx, sql)
	if err != nil {
		return nil, err
	}

	authorIDs := make([]int, 0)

	for rows.Next() {
		var authorID int

		err = rows.Scan(&authorID)
		if err != nil {
			return nil, err
		}

		authorIDs = append(authorIDs, authorID)
	}

	return authorIDs, nil
}

func (f *FakesCreator) insertBooks(ctx context.Context, books []book.Book) error {
	sql, values := prepareQueryBooks(books)

	_, err := f.db.Exec(ctx, sql, values...)

	return err
}

func prepareQueryBooks(books []book.Book) (string, []interface{}) {
	const count = 3

	var sql strings.Builder

	values := make([]interface{}, 0, count*len(books))

	sql.WriteString("INSERT INTO library.books (name, author_id, is_issued) VALUES ")

	for i, book := range books {
		sql.WriteString(fmt.Sprintf("($%d, $%d, $%d),",
			count*i+1, count*i+2, count*i+3))

		values = append(values, book.Name, book.AuthorID, true)
	}

	return sql.String()[:sql.Len()-1], values
}

func getMassiveBooks(num int, authorsIDs []int) []book.Book {
	fakeBooks := make([]book.Book, 0, num)
	listNames := make(map[string]struct{})
	for i := 0; i < num; i++ {
		book := book.Book{
			Name: gofakeit.BookTitle(),
			AuthorID: func() int {
				k := gofakeit.Number(0, len(authorsIDs)-1)

				return authorsIDs[k]
			}(),
		}

		ok := true

		for ok {
			if _, ok = listNames[book.Name]; ok {
				book.Name += ": " + gofakeit.Word()
			}
		}

		fakeBooks = append(fakeBooks, book)
		listNames[book.Name] = struct{}{}
	}

	return fakeBooks
}
