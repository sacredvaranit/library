package validate

import (
	"github.com/go-playground/validator/v10"
	"library/app/internal/models/errors_domain"
)

var validate *validator.Validate

func init() {
	validate = validator.New()
}

func ValidateRequest(request interface{}) error {
	err := validate.Struct(request)
	if err != nil {
		return errors_domain.ErrWrongBodyRequest
	}

	return nil
}
