package context_value

import (
	"context"
	"net/http"
)

const errKey = "err"

func WrapErrorToRequestCtx(r *http.Request, err error) {
	if r == nil {
		return
	}

	*r = *r.WithContext(context.WithValue(r.Context(), errKey, err))
}

func GetErrorFromRequestCtx(r *http.Request) error {
	errValue := r.Context().Value(errKey)

	if errValue == nil {
		return nil
	}

	err := errValue.(error)

	return err
}
