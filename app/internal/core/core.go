package core

import (
	"context"
	"fmt"
	"library/app/internal/middleware/erorr_middleware"
	"library/app/internal/pg"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/jackc/pgx/v5/pgxpool"

	"library/app/internal/config"
	"library/app/internal/handlers/author"
	"library/app/internal/handlers/book"
	"library/app/internal/handlers/rented_books"
	"library/app/internal/handlers/swag"
	"library/app/internal/handlers/user"
	"library/app/internal/lib/fakers"
	author_repo "library/app/internal/repository/postgres/author"
	"library/app/internal/repository/postgres/book"
	rented_repo "library/app/internal/repository/postgres/rented_books"
	user_repo "library/app/internal/repository/postgres/user"
	author_service "library/app/internal/service/author"
	book_service "library/app/internal/service/book"
	"library/app/internal/service/clock"
	rented_service "library/app/internal/service/rented_books"
	user_service "library/app/internal/service/user"
)

type App struct {
	pgPool *pgxpool.Pool
	server *http.Server
}

func NewApp(config *config.Config) App {
	const (
		readTimeout       = 5 * time.Second
		readHeaderTimeout = 5 * time.Second
		writeTimeout      = 5 * time.Second
		idleTimeout       = 5 * time.Second
	)

	ctx := context.Background()
	pgPool, err := pg.NewConn(
		ctx,
		pg.Config{
			Username: config.DatabaseConfig.Username,
			Password: config.DatabaseConfig.Password,
			Host:     config.DatabaseConfig.Host,
			Port:     config.DatabaseConfig.Port,
			Database: config.DatabaseConfig.Database,
		})
	if err != nil {
		log.Fatal(err.Error())
	}

	err = pg.MigratePool(pgPool.GetPool())
	if err != nil {
		log.Fatalf("%w", err)
	}

	clock := clock.NewTimeWatcher()
	router := chi.NewRouter()
	router.Use(erorr_middleware.ErrorMiddleware)

	repo := user_repo.NewRepo(pgPool.GetPool())
	service := user_service.NewService(repo)
	handler := user.NewHandler(service)

	handler.Register(router)

	repoAuthor := author_repo.NewRepo(pgPool.GetPool())
	serviceAuthor := author_service.NewService(repoAuthor)
	handlerAuthor := author.NewHandler(serviceAuthor)
	handlerAuthor.Register(router)

	bookRepo := add_book_db.NewRepo(pgPool.GetPool())
	serviceBook := book_service.NewService(bookRepo)
	handlerBook := book.NewHandler(serviceBook)
	handlerBook.Register(router)

	rentedRepo := rented_repo.NewRepo(pgPool.GetPool())
	rentedService := rented_service.NewService(rentedRepo, clock)
	rentedHandler := rented_books.NewHandler(rentedService)
	rentedHandler.Register(router)

	swaggerHandler := swag.NewSwagHandler()
	swaggerHandler.Register(router)

	server := &http.Server{
		Addr:              fmt.Sprintf("%s:%s", config.ServerConfig.Host, config.ServerConfig.Port),
		Handler:           router,
		ReadTimeout:       readTimeout,
		ReadHeaderTimeout: readHeaderTimeout,
		WriteTimeout:      writeTimeout,
		IdleTimeout:       idleTimeout,
	}

	return App{
		pgPool: pgPool.GetPool(),
		server: server,
	}
}

func (a *App) Run() {
	go func() {
		err := a.server.ListenAndServe()
		if err != nil {
			log.Panic("failed to start server: ", err.Error())
		}
	}()
}

func (a *App) InsertStartData(ctx context.Context, cfg *config.Config) {
	faker := fakers.NewFakesCreator(a.pgPool)

	err := faker.StartUp(ctx)
	if err != nil {
		log.Println("Failed to insert Start data")
		log.Fatal("Failed to insert Start data: ", err.Error())
	}
}

func (a *App) Shutdown() {
	err := a.server.Shutdown(context.Background())
	if err != nil {
		log.Println("failed to shutdown server: ", err.Error())
	}

	a.pgPool.Close()
}
