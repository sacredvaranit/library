package pg

import (
	"embed"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/jackc/pgx/v5/stdlib"
	"github.com/pressly/goose/v3"
)

//go:embed migrations/*.sql
var embedMigrations embed.FS

func MigratePool(pool *pgxpool.Pool) error {
	const (
		dialect = "postgres"
		path    = "migrations"
	)

	goose.SetBaseFS(embedMigrations)

	err := goose.SetDialect(dialect)
	if err != nil {
		return err
	}

	db := stdlib.OpenDBFromPool(pool)

	err = goose.Up(db, path)
	if err != nil {
		return err
	}

	err = db.Close()
	if err != nil {
		return err
	}

	return nil
}
