package pg

type Config struct {
	Username string
	Password string
	Host     string
	Port     string
	Database string
}
