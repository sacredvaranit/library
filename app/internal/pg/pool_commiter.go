package pg

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type PoolCommitter struct {
	pool *pgxpool.Pool
}

func (p *PoolCommitter) InReadTx(ctx context.Context, f func(ctx context.Context, tx pgx.Tx) error) error {
	return p.inTx(ctx, pgx.ReadOnly, f)
}

func (p *PoolCommitter) InWriteTx(ctx context.Context, f func(ctx context.Context, tx pgx.Tx) error) error {
	return p.inTx(ctx, pgx.ReadWrite, f)
}

func (p *PoolCommitter) inTx(ctx context.Context, r pgx.TxAccessMode, f func(ctx context.Context, tx pgx.Tx) error) (err error) {
	var tx pgx.Tx

	tx, err = p.pool.BeginTx(ctx, pgx.TxOptions{AccessMode: r})
	if err != nil {
		return
	}

	defer func() {
		if p := recover(); p != nil {
			_ = tx.Rollback(ctx)

			panic(p) // fallthrough panic after rollback on caught panic
		} else if err != nil {
			_ = tx.Rollback(ctx) // if error during computations
		} else {
			err = tx.Commit(ctx) // all good
		}
	}()

	err = f(ctx, tx)
	return
}

func NewConn(
	ctx context.Context,
	config Config,
) (*PoolCommitter, error) {
	dsn := fmt.Sprintf(
		"postgresql://%s:%s@%s:%s/%s",
		config.Username,
		config.Password,
		config.Host,
		config.Port,
		config.Database,
	)

	pgxConfig, err := pgxpool.ParseConfig(dsn)
	if err != nil {
		return nil, err
	}

	pool, err := pgxpool.NewWithConfig(ctx, pgxConfig)
	if err != nil {
		return nil, err
	}

	err = pool.Ping(ctx)
	if err != nil {
		return nil, err
	}

	return &PoolCommitter{pool}, nil
}

func (p *PoolCommitter) GetPool() *pgxpool.Pool {
	return p.pool
}
