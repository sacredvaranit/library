-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS library.books (
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY ,
    name TEXT NOT NULL UNIQUE ,
    author_id INT NOT NULL REFERENCES library.authors(id),
    is_issued BOOLEAN NOT NULL DEFAULT true,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS library.books;
-- +goose StatementEnd
