-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS library.rented_books (
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    user_id INT NOT NULL REFERENCES library.users(id),
    book_id INT NOT NULL REFERENCES library.books(id),
    issued_at timestamptz NOT NULL,
    expired_at timestamptz NOT NULL,
    returned_at timestamptz
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS library.rented_books;
-- +goose StatementEnd
