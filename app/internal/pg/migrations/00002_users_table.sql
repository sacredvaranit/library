-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS library.users (
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY ,
    first_name TEXT NOT NULL ,
    middle_name TEXT,
    last_name TEXT NOT NULL ,
    birthday DATE NOT NULL ,
    created_at timestamptz NOT NULL default NOW(),
    updated_at timestamptz
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS library.users;
-- +goose StatementEnd
