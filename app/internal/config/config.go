package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/joho/godotenv"
	"log"
)

type ServerConfig struct {
	Host string `env:"HTTP_HOST" env-required:"localhost"`
	Port string `env:"HTTP_PORT" env-required:"8080"`
}

type DatabaseConfig struct {
	Username string `env:"PSQL_USERNAME" env-required:"true"`
	Password string `env:"PSQL_PASSWORD" env-required:"true"`
	Host     string `env:"PSQL_HOST" env-default:"localhost"`
	Port     string `env:"PSQL_PORT" env-default:"5432"`
	Database string `env:"PSQL_DATABASE" env-required:"true"`
}

type Config struct {
	ServerConfig
	DatabaseConfig
}

func GetConfig() (*Config, error) {
	cfg := &Config{}

	err := godotenv.Load()
	if err != nil {
		log.Fatal("failed to read config: ", err.Error())
	}

	err = cleanenv.ReadEnv(cfg)
	if err != nil {

		return nil, err
	}

	return cfg, nil
}
