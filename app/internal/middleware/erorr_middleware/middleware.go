package erorr_middleware

import (
	"encoding/json"
	"errors"
	"library/app/internal/lib/context_value"
	err_controller "library/app/internal/models/controller/err"
	"library/app/internal/models/errors_domain"
	"log"
	"net/http"
)

func ErrorMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		const errorInternalMsg = "Something went wrong"

		next.ServeHTTP(w, req)

		var (
			notFoundError errors_domain.NotFoundError
			domainError   errors_domain.DomainError
		)

		err := context_value.GetErrorFromRequestCtx(req)
		if err == nil {

			return
		}

		resp := err_controller.ErrorResponse{Msg: err.Error()}
		log.Println(resp)
		switch {
		case errors.As(err, &domainError):
			w.WriteHeader(http.StatusBadRequest)
		case errors.As(err, &notFoundError):
			w.WriteHeader(http.StatusNotFound)
		default:
			w.WriteHeader(http.StatusInternalServerError)
			resp.Msg = errorInternalMsg
		}

		response, err := json.Marshal(resp)
		if err != nil {
			return
		}

		w.Write(response)
	})
}
