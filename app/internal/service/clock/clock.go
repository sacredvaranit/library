package clock

import "time"

type TimeWatcher interface {
	Now() time.Time
}

type UTC struct {
}

func (u *UTC) Now() time.Time {
	return time.Now().UTC()
}

func NewTimeWatcher() *UTC {
	return &UTC{}
}
