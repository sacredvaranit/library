package book

import (
	"context"
	"library/app/internal/models/domain/author"
	"library/app/internal/models/domain/book"
	"library/app/internal/models/domain/book_author"
)

type bookManager interface {
	GetBook(ctx context.Context, searchBook book.Book) (book.Book, error)
	GetAuthorByID(ctx context.Context, authorID int) (author.Author, error)
	AddBookAndReturnID(ctx context.Context, book book.Book) (int, error)
	GetBooks(ctx context.Context, authorID int) ([]book_author.BookAuthorAggregator, error)
}

type Service struct {
	repo bookManager
}

func NewService(repo bookManager) *Service {
	return &Service{
		repo: repo,
	}
}
