package book

import (
	"context"
	"errors"
	minimock "github.com/gojuno/minimock/v3"
	"library/app/internal/models/domain/author"
	"library/app/internal/models/errors_domain"
	"testing"

	"library/app/internal/models/domain/book"
)

func TestAddBookService_AddBook(t *testing.T) {
	tests := []struct {
		name    string
		repo    func(ctx context.Context, book book.Book, wantID int, needErr error) bookManager
		ctx     context.Context
		book    book.Book
		err     error
		errRepo error
		wantID  int
		wantErr bool
	}{
		{name: "Without error",
			repo: func(ctx context.Context, searchBook book.Book, wantID int, needErr error) bookManager {
				ctrl := minimock.NewController(t)
				mock := NewBookManagerMock(ctrl)
				mock.GetBookMock.Expect(ctx, searchBook).Return(book.Book{}, nil)
				mock.GetAuthorByIDMock.Expect(ctx, searchBook.AuthorID).Return(author.Author{ID: 1}, nil)
				mock.AddBookAndReturnIDMock.Expect(ctx, searchBook).Return(wantID, needErr)
				return mock
			},
			ctx:     context.Background(),
			book:    book.Book{},
			err:     nil,
			errRepo: nil,
			wantID:  1,
			wantErr: false,
		},

		{name: "Without error, but book already exists",
			repo: func(ctx context.Context, searchBook book.Book, wantID int, needErr error) bookManager {
				ctrl := minimock.NewController(t)
				mock := NewBookManagerMock(ctrl)
				mock.GetBookMock.Expect(ctx, searchBook).Return(book.Book{ID: 1}, nil)
				return mock
			},
			ctx:     context.Background(),
			book:    book.Book{},
			err:     errors_domain.ErrBookExist,
			errRepo: nil,
			wantID:  0,
			wantErr: true,
		},

		{name: "Without error, but author not found",
			repo: func(ctx context.Context, searchBook book.Book, wantID int, needErr error) bookManager {
				ctrl := minimock.NewController(t)
				mock := NewBookManagerMock(ctrl)
				mock.GetBookMock.Expect(ctx, searchBook).Return(book.Book{}, nil)
				mock.GetAuthorByIDMock.Expect(ctx, searchBook.AuthorID).Return(author.Author{}, nil)
				return mock
			},
			ctx:     context.Background(),
			book:    book.Book{},
			err:     errors_domain.ErrAuthorNotFound,
			errRepo: nil,
			wantID:  0,
			wantErr: true,
		},

		{name: "With error repo's method GetBook",
			repo: func(ctx context.Context, searchBook book.Book, wantID int, needErr error) bookManager {
				ctrl := minimock.NewController(t)
				mock := NewBookManagerMock(ctrl)
				mock.GetBookMock.Expect(ctx, searchBook).Return(searchBook, needErr)
				return mock
			},
			ctx:     context.Background(),
			book:    book.Book{ID: 1},
			err:     errors.New("Some error"),
			errRepo: errors.New("Some error"),
			wantID:  0,
			wantErr: true,
		},
		{name: "With error repo's method GetAuthorByID",
			repo: func(ctx context.Context, searchBook book.Book, wantID int, needErr error) bookManager {
				ctrl := minimock.NewController(t)
				mock := NewBookManagerMock(ctrl)
				mock.GetBookMock.Expect(ctx, searchBook).Return(book.Book{}, nil)
				mock.GetAuthorByIDMock.Expect(ctx, searchBook.AuthorID).Return(author.Author{}, needErr)
				return mock
			},
			ctx:     context.Background(),
			book:    book.Book{ID: 1},
			err:     errors.New("Some error"),
			errRepo: errors.New("Some error"),
			wantID:  0,
			wantErr: true,
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			add := NewService(tt.repo(tt.ctx, tt.book, tt.wantID, tt.errRepo))
			if bookID, err := add.AddBook(tt.ctx, tt.book); (err != nil) != tt.wantErr || bookID != tt.wantID {
				t.Errorf("AddBook() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
