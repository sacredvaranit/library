package book

import (
	"context"
	"library/app/internal/models/domain/book"
	"library/app/internal/models/errors_domain"
)

func (s *Service) AddBook(ctx context.Context, book book.Book) (int, error) {
	findBook, err := s.repo.GetBook(ctx, book)
	if err != nil {
		return 0, err
	}

	if findBook.ID != 0 {
		return 0, errors_domain.ErrBookExist
	}

	authorExist, err := s.repo.GetAuthorByID(ctx, book.AuthorID)
	if authorExist.ID == 0 {
		return 0, errors_domain.ErrAuthorNotFound
	}

	return s.repo.AddBookAndReturnID(ctx, book)
}
