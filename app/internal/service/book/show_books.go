package book

import (
	"context"
	"library/app/internal/models/errors_domain"

	"library/app/internal/models/domain/book_author"
)

func (s *Service) ShowBooks(ctx context.Context, authorID int) ([]book_author.BookAuthorAggregator, error) {
	if authorID != 0 {
		author, err := s.repo.GetAuthorByID(ctx, authorID)
		if err != nil {
			return nil, err
		}

		if author.ID == 0 {
			return nil, errors_domain.ErrAuthorNotFound
		}
	}

	books, err := s.repo.GetBooks(ctx, authorID)
	if err != nil {
		return nil, err
	}

	if len(books) == 0 {
		return nil, errors_domain.ErrBookNotFound
	}

	return books, nil
}
