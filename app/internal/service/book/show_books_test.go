package book

import (
	"context"
	"errors"
	"library/app/internal/models/domain/author"
	"library/app/internal/models/domain/book"
	"library/app/internal/models/errors_domain"
	"reflect"
	"testing"

	minimock "github.com/gojuno/minimock/v3"

	"library/app/internal/models/domain/book_author"
)

func TestAddBookService_ShowBooks(t *testing.T) {
	tests := []struct {
		name     string
		repo     func(ctx context.Context, bookID int, wantBooks []book_author.BookAuthorAggregator, err error) bookManager
		ctx      context.Context
		want     []book_author.BookAuthorAggregator
		authorID int
		err      error
		errRepo  error
		wantErr  bool
	}{
		{name: "Without errors",
			repo: func(ctx context.Context, authorID int, wantBooks []book_author.BookAuthorAggregator, err error) bookManager {
				ctrl := minimock.NewController(t)
				mock := NewBookManagerMock(ctrl)
				mock.GetAuthorByIDMock.Expect(ctx, authorID).Return(author.Author{ID: 1}, err)
				mock.GetBooksMock.Expect(ctx, authorID).Return(wantBooks, err)
				return mock
			},
			ctx:      context.Background(),
			want:     []book_author.BookAuthorAggregator{{Book: book.Book{AuthorID: 1}}},
			authorID: 1,
			err:      nil,
			errRepo:  nil,
			wantErr:  false},

		{name: "Without errors but books not found",
			repo: func(ctx context.Context, authorID int, wantBooks []book_author.BookAuthorAggregator, err error) bookManager {
				ctrl := minimock.NewController(t)
				mock := NewBookManagerMock(ctrl)
				mock.GetAuthorByIDMock.Expect(ctx, authorID).Return(author.Author{ID: 1}, err)
				mock.GetBooksMock.Expect(ctx, authorID).Return([]book_author.BookAuthorAggregator{}, err)
				return mock
			},
			ctx:      context.Background(),
			want:     nil,
			authorID: 1,
			err:      errors_domain.ErrBookNotFound,
			errRepo:  nil,
			wantErr:  true},

		{name: "Without errors but author not found",
			repo: func(ctx context.Context, authorID int, wantBooks []book_author.BookAuthorAggregator, err error) bookManager {
				ctrl := minimock.NewController(t)
				mock := NewBookManagerMock(ctrl)
				mock.GetAuthorByIDMock.Expect(ctx, authorID).Return(author.Author{}, err)
				return mock
			},
			ctx:      context.Background(),
			want:     nil,
			authorID: 1,
			err:      errors_domain.ErrAuthorNotFound,
			errRepo:  nil,
			wantErr:  true},

		{name: "With err repo's method GetAuthor",
			repo: func(ctx context.Context, authorID int, wantBooks []book_author.BookAuthorAggregator, err error) bookManager {
				ctrl := minimock.NewController(t)
				mock := NewBookManagerMock(ctrl)
				mock.GetAuthorByIDMock.Expect(ctx, authorID).Return(author.Author{}, err)
				return mock
			},
			ctx:      context.Background(),
			want:     nil,
			authorID: 1,
			err:      errors.New("Some error"),
			errRepo:  errors.New("Some error"),
			wantErr:  true},

		{name: "With err repo's method GetBooks",
			repo: func(ctx context.Context, authorID int, wantBooks []book_author.BookAuthorAggregator, err error) bookManager {
				ctrl := minimock.NewController(t)
				mock := NewBookManagerMock(ctrl)
				mock.GetAuthorByIDMock.Expect(ctx, authorID).Return(author.Author{ID: 1}, nil)
				mock.GetBooksMock.Expect(ctx, authorID).Return(nil, err)
				return mock
			},
			ctx:      context.Background(),
			want:     nil,
			authorID: 1,
			err:      errors.New("Some error"),
			errRepo:  errors.New("Some error"),
			wantErr:  true},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewService(tt.repo(tt.ctx, tt.authorID, tt.want, tt.errRepo))
			got, err := s.ShowBooks(tt.ctx, tt.authorID)
			if (err != nil) != tt.wantErr {
				t.Errorf("ShowBooks() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ShowBooks() got = %v, want %v", got, tt.want)
			}
		})
	}
}
