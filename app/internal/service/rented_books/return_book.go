package rented_books

import (
	"context"
	"library/app/internal/models/errors_domain"
)

func (u *Service) ReturnBook(ctx context.Context, userID, bookID int) (int, error) {
	isAvaliable, err := u.UserRepo.CheckRentedBook(ctx, userID, bookID)
	if err != nil {

		return 0, err
	}

	if !isAvaliable {

		return 0, errors_domain.ErrNoRentedBookByUserErr
	}

	return u.UserRepo.ReturnedBookAndReturnID(ctx, userID, bookID, u.Clock.Now())
}
