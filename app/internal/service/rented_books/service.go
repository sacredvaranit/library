package rented_books

import (
	"context"
	"library/app/internal/models/domain/book"
	"library/app/internal/models/domain/user"
	"time"
)

type rentedBookManager interface {
	ReturnedBookAndReturnID(ctx context.Context, userID, bookID int, timeReturn time.Time) (int, error)
	InsertToRentedAndReturnID(ctx context.Context, userID, bookID int, timeCreate, timeRented time.Time) (int, error)
	FindUserByID(ctx context.Context, userID int) (user.User, error)
	FindBookByID(ctx context.Context, bookID int) (book.Book, error)
	CheckRentedBook(ctx context.Context, userID, bookID int) (bool, error)
}

type TimeWatcher interface {
	Now() time.Time
}

type Service struct {
	UserRepo rentedBookManager
	Clock    TimeWatcher
}

func NewService(userRepo rentedBookManager, clock TimeWatcher) *Service {
	return &Service{
		UserRepo: userRepo,
		Clock:    clock,
	}
}
