package rented_books

import (
	"context"
	"errors"
	"library/app/internal/models/domain/book"
	"library/app/internal/models/domain/user"
	"testing"
	"time"

	minimock "github.com/gojuno/minimock/v3"
)

func TestRentedService_TakeBook(t *testing.T) {
	tests := []struct {
		name     string
		UserRepo func(
			ctx context.Context,
			userID, bookID int,
			wantID int,
			time TimeWatcher,
			errFindBook,
			errFindUser,
			errInsert error) rentedBookManager
		Clock       func() TimeWatcher
		ctx         context.Context
		userID      int
		bookID      int
		wantID      int
		wantErr     bool
		errFindBook error
		errFindUser error
		errInsert   error
	}{
		{name: "Without errors",
			UserRepo: func(
				ctx context.Context,
				userID, bookID int,
				wantID int,
				timer TimeWatcher,
				errFindBook,
				errFindUser,
				errInsert error) rentedBookManager {
				ctrl := minimock.NewController(t)
				mock := NewRentedBookManagerMock(ctrl)
				mock.FindUserByIDMock.Expect(ctx, userID).Return(user.User{ID: userID}, errFindUser)
				mock.FindBookByIDMock.Expect(ctx, bookID).Return(book.Book{ID: bookID, IsIssued: true}, errFindBook)
				mock.InsertToRentedAndReturnIDMock.
					Expect(ctx, userID, bookID, timer.Now(), timer.Now().AddDate(0, 0, 5)).
					Return(wantID, errInsert)
				return mock
			},
			Clock: func() TimeWatcher {
				ctrl := minimock.NewController(t)
				mock := NewTimeWatcherMock(ctrl)
				mock.NowMock.Expect().Return(time.Time{})
				return mock
			},
			ctx:         context.Background(),
			userID:      1,
			bookID:      1,
			wantID:      1,
			wantErr:     false,
			errFindUser: nil,
			errFindBook: nil,
			errInsert:   nil},

		{name: "With errors at find user",
			UserRepo: func(
				ctx context.Context,
				userID, bookID int,
				wantID int,
				timer TimeWatcher,
				errFindBook, errFindUser, errInsert error) rentedBookManager {
				ctrl := minimock.NewController(t)
				mock := NewRentedBookManagerMock(ctrl)
				mock.FindUserByIDMock.Expect(ctx, userID).Return(user.User{}, errFindUser)
				return mock
			},
			Clock: func() TimeWatcher {
				return nil
			},
			ctx:         context.Background(),
			userID:      1,
			bookID:      1,
			wantErr:     true,
			errFindUser: errors.New("Error"),
			errFindBook: nil,
			errInsert:   nil},

		{name: "Without errors_domain, but user not found",
			UserRepo: func(
				ctx context.Context,
				userID, bookID int,
				wantID int,
				timer TimeWatcher,
				errFindBook, errFindUser, errInsert error) rentedBookManager {
				ctrl := minimock.NewController(t)
				mock := NewRentedBookManagerMock(ctrl)
				mock.FindUserByIDMock.Expect(ctx, userID).Return(user.User{}, errFindUser)
				return mock
			},
			Clock: func() TimeWatcher {
				return nil
			},
			ctx:         context.Background(),
			userID:      1,
			bookID:      1,
			wantErr:     true,
			errFindUser: nil,
			errFindBook: nil,
			errInsert:   nil},

		{name: "With errors at find book",
			UserRepo: func(
				ctx context.Context,
				userID, bookID int,
				wantID int,
				timer TimeWatcher,
				errFindBook, errFindUser, errInsert error) rentedBookManager {
				ctrl := minimock.NewController(t)
				mock := NewRentedBookManagerMock(ctrl)
				mock.FindUserByIDMock.Expect(ctx, userID).Return(user.User{ID: userID}, errFindUser)
				mock.FindBookByIDMock.Expect(ctx, bookID).Return(book.Book{}, errFindBook)
				return mock
			},
			Clock: func() TimeWatcher {
				return nil
			},
			ctx:         context.Background(),
			userID:      1,
			bookID:      1,
			wantErr:     true,
			errFindUser: nil,
			errFindBook: errors.New("Error"),
			errInsert:   nil},

		{name: "Without errors, but book not found",
			UserRepo: func(
				ctx context.Context,
				userID, bookID int,
				wantID int,
				timer TimeWatcher,
				errFindBook, errFindUser, errInsert error) rentedBookManager {
				ctrl := minimock.NewController(t)
				mock := NewRentedBookManagerMock(ctrl)
				mock.FindUserByIDMock.Expect(ctx, userID).Return(user.User{ID: userID}, errFindUser)
				mock.FindBookByIDMock.Expect(ctx, bookID).Return(book.Book{}, errFindBook)
				return mock
			},
			Clock: func() TimeWatcher {
				return nil
			},
			ctx:         context.Background(),
			userID:      1,
			bookID:      1,
			wantErr:     true,
			errFindUser: nil,
			errFindBook: nil,
			errInsert:   nil},

		{name: "Without errors, but book already rent",
			UserRepo: func(
				ctx context.Context,
				userID, bookID int,
				wantID int,
				timer TimeWatcher,
				errFindBook, errFindUser, errInsert error) rentedBookManager {
				ctrl := minimock.NewController(t)
				mock := NewRentedBookManagerMock(ctrl)
				mock.FindUserByIDMock.Expect(ctx, userID).Return(user.User{ID: userID}, errFindUser)
				mock.FindBookByIDMock.Expect(ctx, bookID).Return(book.Book{ID: bookID, IsIssued: false}, errFindBook)
				return mock
			},
			Clock: func() TimeWatcher {
				return nil
			},
			ctx:         context.Background(),
			userID:      1,
			bookID:      1,
			wantErr:     true,
			errFindUser: nil,
			errFindBook: nil,
			errInsert:   nil},

		{name: "With errors at insert to rented",
			UserRepo: func(
				ctx context.Context,
				userID, bookID int,
				wantID int,
				timer TimeWatcher,
				errFindBook, errFindUser, errInsert error) rentedBookManager {
				ctrl := minimock.NewController(t)
				mock := NewRentedBookManagerMock(ctrl)
				mock.FindUserByIDMock.Expect(ctx, userID).Return(user.User{ID: userID}, errFindUser)
				mock.FindBookByIDMock.Expect(ctx, bookID).Return(book.Book{ID: bookID, IsIssued: true}, errFindBook)
				mock.InsertToRentedAndReturnIDMock.
					Expect(ctx, userID, bookID, timer.Now(), timer.Now().AddDate(0, 0, 5)).
					Return(wantID, errInsert)
				return mock
			},
			Clock: func() TimeWatcher {
				ctrl := minimock.NewController(t)
				mock := NewTimeWatcherMock(ctrl)
				mock.NowMock.Expect().Return(time.Time{})
				return mock
			},
			wantID:      0,
			ctx:         context.Background(),
			userID:      1,
			bookID:      1,
			wantErr:     true,
			errFindUser: nil,
			errFindBook: nil,
			errInsert:   errors.New("Error")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := NewService(
				tt.UserRepo(
					tt.ctx,
					tt.userID,
					tt.bookID,
					tt.wantID,
					tt.Clock(),
					tt.errFindBook,
					tt.errFindUser,
					tt.errInsert),
				tt.Clock())
			if entryID, err := u.TakeBook(tt.ctx, tt.userID, tt.bookID); (err != nil) != tt.wantErr || entryID != tt.wantID {
				t.Errorf("TakeBook() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
