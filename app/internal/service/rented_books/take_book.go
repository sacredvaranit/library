package rented_books

import (
	"context"
	"library/app/internal/models/errors_domain"
)

func (u *Service) TakeBook(ctx context.Context, userID, bookID int) (int, error) {
	userExist, err := u.UserRepo.FindUserByID(ctx, userID)
	if err != nil {
		return 0, err
	}

	if userExist.ID == 0 {
		return 0, errors_domain.ErrUserNotFound
	}

	bookExist, err := u.UserRepo.FindBookByID(ctx, bookID)
	if err != nil {
		return 0, err
	}

	if bookExist.ID == 0 {
		return 0, errors_domain.ErrBookNotFound
	}

	if !bookExist.IsIssued {
		return 0, errors_domain.ErrBookAlreadyRent
	}

	return u.UserRepo.InsertToRentedAndReturnID(
		ctx,
		userID,
		bookID,
		u.Clock.Now(),
		u.Clock.Now().AddDate(0, 0, 5),
	)
}
