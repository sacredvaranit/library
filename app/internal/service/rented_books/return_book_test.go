package rented_books

import (
	"context"
	"errors"
	"testing"
	"time"

	minimock "github.com/gojuno/minimock/v3"
)

func TestRentedService_ReturnBook(t *testing.T) {
	tests := []struct {
		name      string
		UserRepo  func(ctx context.Context, userID, bookID int, wantID int, time TimeWatcher, errFind, errUpdate error) rentedBookManager
		Clock     func() TimeWatcher
		ctx       context.Context
		userID    int
		bookID    int
		wantID    int
		wantErr   bool
		errFind   error
		errUpdate error
	}{
		{name: "Without errors",
			UserRepo: func(
				ctx context.Context, userID, bookID int, wantID int, time TimeWatcher, errFind, errUpdate error) rentedBookManager {
				ctrl := minimock.NewController(t)
				mock := NewRentedBookManagerMock(ctrl)
				mock.CheckRentedBookMock.Expect(ctx, userID, bookID).Return(true, errFind)
				mock.ReturnedBookAndReturnIDMock.Expect(ctx, userID, bookID, time.Now()).Return(wantID, errUpdate)
				return mock
			},
			Clock: func() TimeWatcher {
				ctrl := minimock.NewController(t)
				mock := NewTimeWatcherMock(ctrl)
				mock.NowMock.Expect().Return(time.Time{})
				return mock
			},
			ctx:       context.Background(),
			userID:    1,
			bookID:    1,
			wantID:    1,
			wantErr:   false,
			errFind:   nil,
			errUpdate: nil},

		{name: "With errors in check",
			UserRepo: func(
				ctx context.Context, userID, bookID int, wantID int, time TimeWatcher, errFind, errUpdate error) rentedBookManager {
				ctrl := minimock.NewController(t)
				mock := NewRentedBookManagerMock(ctrl)
				mock.CheckRentedBookMock.Expect(ctx, userID, bookID).Return(false, errFind)
				return mock
			},
			Clock: func() TimeWatcher {
				return nil
			},
			ctx:       context.Background(),
			userID:    1,
			bookID:    1,
			wantErr:   true,
			errFind:   errors.New("Error"),
			errUpdate: nil},

		{name: "Without errors, but failure at find rented",
			UserRepo: func(
				ctx context.Context, userID, bookID int, wantID int, time TimeWatcher, errFind, errUpdate error) rentedBookManager {
				ctrl := minimock.NewController(t)
				mock := NewRentedBookManagerMock(ctrl)
				mock.CheckRentedBookMock.Expect(ctx, userID, bookID).Return(false, errFind)
				return mock
			},
			Clock: func() TimeWatcher {
				return nil
			},
			ctx:       context.Background(),
			userID:    1,
			bookID:    1,
			wantErr:   true,
			errFind:   nil,
			errUpdate: nil},

		{name: "With errors_domain at update",
			UserRepo: func(
				ctx context.Context, userID, bookID int, wantID int, time TimeWatcher, errFind, errUpdate error) rentedBookManager {
				ctrl := minimock.NewController(t)
				mock := NewRentedBookManagerMock(ctrl)
				mock.CheckRentedBookMock.Expect(ctx, userID, bookID).Return(true, errFind)
				mock.ReturnedBookAndReturnIDMock.Expect(ctx, userID, bookID, time.Now()).Return(wantID, errUpdate)
				return mock
			},
			Clock: func() TimeWatcher {
				ctrl := minimock.NewController(t)
				mock := NewTimeWatcherMock(ctrl)
				mock.NowMock.Expect().Return(time.Time{})
				return mock
			},
			ctx:       context.Background(),
			userID:    1,
			bookID:    1,
			wantErr:   true,
			errFind:   nil,
			errUpdate: errors.New("Error")},
		// TODO: Add test cases.
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := NewService(
				tt.UserRepo(tt.ctx, tt.userID, tt.bookID, tt.wantID, tt.Clock(), tt.errFind, tt.errUpdate),
				tt.Clock())
			if entryID, err := u.ReturnBook(tt.ctx, tt.userID, tt.bookID); (err != nil) != tt.wantErr || entryID != tt.wantID {
				t.Errorf("ReturnBook() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
