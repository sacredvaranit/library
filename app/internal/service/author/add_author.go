package author

import (
	"context"
	"library/app/internal/models/domain/author"
	error_models "library/app/internal/models/errors_domain"
)

func (a *Service) AddAuthor(ctx context.Context, author author.Author) (int, error) {
	authorGeted, err := a.repo.GetAuthor(ctx, author)
	if err != nil {

		return 0, err
	}

	if authorGeted.ID != 0 {

		return 0, error_models.ErrAuthorExist
	}

	return a.repo.AddAuthorAndReturnID(ctx, author)
}
