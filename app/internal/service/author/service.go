package author

import (
	"context"
	"library/app/internal/models/domain/author"
	"library/app/internal/models/domain/author_book"
)

type authorManager interface {
	GetAuthor(ctx context.Context, authorData author.Author) (author.Author, error)
	AddAuthorAndReturnID(ctx context.Context, author author.Author) (int, error)
	GetAuthors(ctx context.Context, auhtorID int) ([]author_book.AuthorBooksAggregator, error)
}

type Service struct {
	repo authorManager
}

func NewService(repo authorManager) *Service {
	return &Service{
		repo: repo,
	}
}
