package author

import (
	"context"
	"errors"
	minimock "github.com/gojuno/minimock/v3"
	"library/app/internal/models/domain/author"
	error_models "library/app/internal/models/errors_domain"

	"testing"
)

func TestAuthorService_AddAuthor(t *testing.T) {

	tests := []struct {
		name    string
		repo    func(ctx context.Context, author author.Author, err error, wantID int) authorManager
		ctx     context.Context
		author  author.Author
		err     error
		errRepo error
		wantID  int
		wantErr bool
	}{
		{name: "Without errors",
			repo: func(ctx context.Context, authorData author.Author, err error, wantID int) authorManager {
				ctrl := minimock.NewController(t)
				mock := NewAuthorManagerMock(ctrl)
				mock.GetAuthorMock.Expect(ctx, authorData).Return(author.Author{}, err)
				mock.AddAuthorAndReturnIDMock.Expect(ctx, authorData).Return(wantID, err)
				return mock
			},
			ctx:     context.Background(),
			author:  author.Author{},
			err:     nil,
			errRepo: nil,
			wantErr: false},

		{name: "Without errors, but author already exists",
			repo: func(ctx context.Context, authorData author.Author, err error, wantID int) authorManager {
				ctrl := minimock.NewController(t)
				mock := NewAuthorManagerMock(ctrl)
				mock.GetAuthorMock.Expect(ctx, authorData).Return(authorData, err)
				return mock
			},
			ctx:     context.Background(),
			author:  author.Author{ID: 1},
			errRepo: nil,
			err:     error_models.ErrAuthorExist,
			wantErr: true},

		{name: "Error by repo's method",
			repo: func(ctx context.Context, authorData author.Author, err error, wantID int) authorManager {
				ctrl := minimock.NewController(t)
				mock := NewAuthorManagerMock(ctrl)
				mock.GetAuthorMock.Expect(ctx, authorData).Return(authorData, err)
				return mock
			},
			ctx:     context.Background(),
			author:  author.Author{ID: 1},
			errRepo: errors.New("Some error"),
			err:     errors.New("Some error"),
			wantErr: true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := NewService(
				tt.repo(tt.ctx, tt.author, tt.errRepo, tt.wantID),
			)
			if authorID, err := a.AddAuthor(tt.ctx, tt.author); (err != nil) != tt.wantErr || authorID != tt.wantID {
				t.Errorf("AddAuthor() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
