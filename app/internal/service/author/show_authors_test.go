package author

import (
	"context"
	"errors"
	"library/app/internal/models/domain/author"
	"library/app/internal/models/errors_domain"
	"reflect"
	"testing"

	minimock "github.com/gojuno/minimock/v3"

	"library/app/internal/models/domain/author_book"
)

func TestAuthorService_ShowAuthors(t *testing.T) {
	tests := []struct {
		name     string
		repo     func(ctx context.Context, authorID int, want []author_book.AuthorBooksAggregator, err error) authorManager
		ctx      context.Context
		authorID int
		want     []author_book.AuthorBooksAggregator
		errRepo  error
		err      error
		wantErr  bool
	}{
		{name: "Without errors_domain",
			repo: func(ctx context.Context, authorID int, want []author_book.AuthorBooksAggregator, err error) authorManager {
				ctrl := minimock.NewController(t)
				mock := NewAuthorManagerMock(ctrl)
				mock.GetAuthorsMock.Expect(ctx, authorID).Return(want, err)
				return mock
			},
			ctx:      context.Background(),
			authorID: 1,
			want:     []author_book.AuthorBooksAggregator{{Author: author.Author{ID: 1}}},
			errRepo:  nil,
			err:      nil,
			wantErr:  false,
		},

		{name: "Without errors, but author not found",
			repo: func(ctx context.Context, authorID int, want []author_book.AuthorBooksAggregator, err error) authorManager {
				ctrl := minimock.NewController(t)
				mock := NewAuthorManagerMock(ctrl)
				mock.GetAuthorsMock.Expect(ctx, authorID).Return(want, err)
				return mock
			},
			ctx:      context.Background(),
			authorID: 1,
			want:     nil,
			errRepo:  nil,
			err:      errors_domain.ErrAuthorNotFound,
			wantErr:  true,
		},

		{name: "With error in GetAuthors",
			repo: func(ctx context.Context, authorID int, want []author_book.AuthorBooksAggregator, err error) authorManager {
				ctrl := minimock.NewController(t)
				mock := NewAuthorManagerMock(ctrl)
				mock.GetAuthorsMock.Expect(ctx, authorID).Return(want, err)
				return mock
			},
			ctx:      context.Background(),
			authorID: 1,
			want:     nil,
			errRepo:  errors.New("Some error"),
			err:      errors.New("Some error"),
			wantErr:  true,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := NewService(tt.repo(tt.ctx, tt.authorID, tt.want, tt.errRepo))
			got, err := a.ShowAuthors(tt.ctx, tt.authorID)
			if (err != nil) != tt.wantErr {
				t.Errorf("ShowAuthors() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ShowAuthors() got = %v, want %v", got, tt.want)
			}
		})
	}
}
