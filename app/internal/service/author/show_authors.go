package author

import (
	"context"
	"library/app/internal/models/errors_domain"

	"library/app/internal/models/domain/author_book"
)

func (a *Service) ShowAuthors(ctx context.Context, authorID int) ([]author_book.AuthorBooksAggregator, error) {
	authors, err := a.repo.GetAuthors(ctx, authorID)
	if err != nil {
		return nil, err
	}

	if len(authors) == 0 {
		return nil, errors_domain.ErrAuthorNotFound
	}

	return authors, nil
}
