package user

import (
	"context"
	"library/app/internal/models/domain/user"
	"library/app/internal/models/domain/user_book_aggregator"
)

type userManager interface {
	GetUser(ctx context.Context, searchUser user.User) (user.User, error)
	InsertUsersAndReturnID(ctx context.Context, user user.User) (int, error)
	SelectUsers(ctx context.Context, userID int) ([]user_book_aggregator.UserRentedBookAggregator, error)
}

type Service struct {
	UserRepo userManager
}

func NewService(userRepo userManager) *Service {
	return &Service{
		UserRepo: userRepo,
	}
}
