package user

import (
	"context"
	"errors"
	"library/app/internal/models/domain/user"
	"library/app/internal/models/errors_domain"
	"reflect"
	"testing"

	minimock "github.com/gojuno/minimock/v3"

	"library/app/internal/models/domain/user_book_aggregator"
)

func TestUserService_ShowUsers(t *testing.T) {
	tests := []struct {
		name     string
		UserRepo func(
			ctx context.Context,
			userID int,
			want []user_book_aggregator.UserRentedBookAggregator,
			err error) userManager
		ctx     context.Context
		want    []user_book_aggregator.UserRentedBookAggregator
		userID  int
		errRepo error
		err     error
		wantErr bool
	}{
		{name: "Without errors",
			UserRepo: func(
				ctx context.Context,
				userID int,
				want []user_book_aggregator.UserRentedBookAggregator,
				err error) userManager {
				ctrl := minimock.NewController(t)
				mock := NewUserManagerMock(ctrl)
				mock.SelectUsersMock.Expect(ctx, userID).Return(want, err)
				return mock
			},
			ctx:     context.Background(),
			userID:  1,
			want:    []user_book_aggregator.UserRentedBookAggregator{{User: user.User{ID: 1}}},
			err:     nil,
			errRepo: nil,
			wantErr: false,
		},

		{name: "Without errors, but user not found",
			UserRepo: func(
				ctx context.Context,
				userID int,
				want []user_book_aggregator.UserRentedBookAggregator,
				err error) userManager {
				ctrl := minimock.NewController(t)
				mock := NewUserManagerMock(ctrl)
				mock.SelectUsersMock.Expect(ctx, userID).Return([]user_book_aggregator.UserRentedBookAggregator{}, err)
				return mock
			},
			ctx:     context.Background(),
			userID:  1,
			want:    nil,
			err:     errors_domain.ErrUserNotFound,
			errRepo: nil,
			wantErr: true,
		},

		{name: "With err repo's method",
			UserRepo: func(
				ctx context.Context,
				userID int,
				want []user_book_aggregator.UserRentedBookAggregator,
				err error) userManager {
				ctrl := minimock.NewController(t)
				mock := NewUserManagerMock(ctrl)
				mock.SelectUsersMock.Expect(ctx, userID).Return(nil, err)
				return mock
			},
			ctx:     context.Background(),
			userID:  1,
			want:    nil,
			err:     errors.New("Some error"),
			errRepo: errors.New("Some error"),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewService(tt.UserRepo(tt.ctx, tt.userID, tt.want, tt.errRepo))
			got, err := s.GetUsers(tt.ctx, tt.userID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetUsers() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetUsers() got = %v, want %v", got, tt.want)
			}
		})
	}
}
