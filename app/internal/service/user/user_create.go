package user

import (
	"context"
	"library/app/internal/models/domain/user"
	"library/app/internal/models/errors_domain"
)

func (s Service) CreateUser(ctx context.Context, user user.User) (int, error) {
	
	findUser, err := s.UserRepo.GetUser(ctx, user)
	if err != nil {

		return 0, err
	}

	if findUser.ID != 0 {

		return 0, errors_domain.ErrUserExist
	}
	return s.UserRepo.InsertUsersAndReturnID(ctx, user)
}
