package user

import (
	"context"
	"errors"
	minimock "github.com/gojuno/minimock/v3"
	"library/app/internal/models/errors_domain"
	"testing"

	"library/app/internal/models/domain/user"
)

func TestUserService_CreateUser(t *testing.T) {
	tests := []struct {
		name     string
		UserRepo func(ctx context.Context, user user.User, id int, err error) userManager
		ctx      context.Context
		user     user.User
		want     int
		errRepo  error
		err      error
		wantErr  bool
	}{
		{name: "Without errors",
			UserRepo: func(ctx context.Context, userData user.User, id int, err error) userManager {
				ctrl := minimock.NewController(t)
				mock := NewUserManagerMock(ctrl)
				mock.GetUserMock.Expect(ctx, userData).Return(user.User{}, nil)
				mock.InsertUsersAndReturnIDMock.Expect(ctx, userData).Return(id, err)
				return mock
			},
			ctx:     context.Background(),
			user:    user.User{},
			want:    1,
			errRepo: nil,
			err:     nil,
			wantErr: false},

		{name: "Without errors, but user already exist",
			UserRepo: func(ctx context.Context, userData user.User, id int, err error) userManager {
				ctrl := minimock.NewController(t)
				mock := NewUserManagerMock(ctrl)
				mock.GetUserMock.Expect(ctx, userData).Return(user.User{ID: 1}, err)
				return mock
			},
			ctx:     context.Background(),
			user:    user.User{},
			want:    0,
			errRepo: nil,
			err:     errors_domain.ErrUserExist,
			wantErr: true},

		{name: "With err by GetUser",
			UserRepo: func(ctx context.Context, userData user.User, id int, err error) userManager {
				ctrl := minimock.NewController(t)
				mock := NewUserManagerMock(ctrl)
				mock.GetUserMock.Expect(ctx, userData).Return(user.User{}, err)
				return mock
			},
			ctx:     context.Background(),
			user:    user.User{},
			want:    0,
			errRepo: errors.New("Some error"),
			err:     errors.New("Some error"),
			wantErr: true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewService(
				tt.UserRepo(tt.ctx, tt.user, tt.want, tt.errRepo))
			got, err := s.CreateUser(tt.ctx, tt.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("CreateUser() got = %v, want %v", got, tt.want)
			}
		})
	}
}
