package user

import (
	"context"
	"library/app/internal/models/errors_domain"

	"library/app/internal/models/domain/user_book_aggregator"
)

func (s *Service) GetUsers(ctx context.Context, userID int) ([]user_book_aggregator.UserRentedBookAggregator, error) {
	users, err := s.UserRepo.SelectUsers(ctx, userID)
	if err != nil {
		return nil, err
	}

	if len(users) == 0 {
		return nil, errors_domain.ErrUserNotFound
	}

	return users, nil
}
